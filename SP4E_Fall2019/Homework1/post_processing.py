"""
October 2019
Authors: Omid Ashtari and Armand Sieber
Description: Scripts intended to display minimization proplems results
"""

import numpy as np

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

# Objective function to be minimized
def objective(X, *args):
    A = args[0]
    B = args[1].T
    S = np.dot(X.T, np.dot(A, X)) - np.dot(X.T, B)
    return S

# The plot function (to be used in other .py files)
def plot_results(X_int, A, B, method):

    # Pre-processing the data to be dispalyed
    ##########################################

    # Set domain size and coordinates according to intermediate solution of minimization problem 
    x_min = np.min(X_int[:, 0])
    x_max = np.max(X_int[:, 0])
    y_min = np.min(X_int[:, 1])
    y_max = np.max(X_int[:, 1])
    X_domain = np.linspace(x_min-0.75*abs(x_max), x_max+0.75*abs(x_max), 100)
    Y_domain = np.linspace(y_min-0.75*abs(x_max), y_max+0.75*abs(x_max), 100)
    xx, yy = np.meshgrid(X_domain, Y_domain)

    # Compute S for the computational domain (S_post) and intermediate minimization solution (S_int)
    S_post = np.array([objective(np.array([x , y]), A, B)
                    for x, y in zip(np.ravel(xx), np.ravel(yy))]) # Value of S for 3D plot (whole X-Y domain)
    S_post = S_post.reshape(xx.shape)

    S_int = np.array([objective(np.array([x , y]), A, B)
                    for x, y in zip(X_int[:,0], X_int[:,1])]) # Value of S at intermediate minimization steps

    # Dispalying the results
    ########################

    # 3D plot
    fig = plt.figure(1)
    ax = plt.axes(projection = '3d')
    ax.plot3D(X_int[:,0], X_int[:,1], S_int, 'r--o', alpha = 1.0, zorder = 10)
    surface = ax.plot_surface(xx, yy, S_post, cmap = 'coolwarm', edgecolor = 'none', alpha = 1.0, zorder = 0) # zorder 'best' solution found to plot S_int over S_post
                                                                                                              # --> alternative using mayavi
    # Axis labels
    ax.set_xlabel('$x$', fontsize = 14)
    ax.set_ylabel('$y$', fontsize = 14)
    ax.set_zlabel('$S$', fontsize = 14)

    # Camera position
    ax.view_init(elev = 60.0, azim = 130.0)

    # Title of the window
    ax.set_title('Minimization method: %s' % method, fontsize = 14, pad = 32)
    plt.savefig('3D_representation.pdf')

    # 2D contour plot
    fig = plt.figure(2)
    ax = fig.add_subplot(111)
    ax.contour(xx, yy, S_post, colors = 'k', levels = 20)
    contour = ax.contourf(xx, yy, S_post, cmap = 'coolwarm', levels = 20)
    ax.plot(X_int[:,0], X_int[:,1], 'r--o')

    # Axis labels
    ax.set_xlabel('$x$', fontsize = 14)
    ax.set_ylabel('$y$', fontsize = 14)

    # Title of the window
    ax.set_title('Minimization method: %s' % method, fontsize = 14)
    fig.colorbar(contour, ax=ax)
    plt.savefig('2D_projection.pdf')

    plt.show()

    return 0
