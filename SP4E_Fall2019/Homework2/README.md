
# SP4E - Homework 2

----

## General Info

This file provides a brief documentation and information related to the second Homework of the course "Scientific Programming for Engineers", fall 2019.

This homework is done by **O. Ashtari** and **A. Sieber**.

Last update: 10.30.2019

----

## Project Description

The aim of this project is to implement a familly of objects intended to compute two types of series: an arithmetic series and a series to approximate the value of pi number. The user can then decide how to dump the results by either printing them to the screen or writing them to a file. If the later option is chosen, a python file is available to plot the results stored in the file.

----

## Executable File

After cloning this repository on your computer, you should build the program in a desired directory based on the `CMakeLists.txt` file in the root. To do so, inside your destination directory, you can build the executable file using commands:
```
$ cmake <location of CMakesLists.txt>
$ make
```
where the command `cmake` is followed by the address of the directory where the CMakeLists.txt is located. The executable file `main` will be built in `<build_directory>/src`.  The minimum requirement for CMake is the version 2.6.

----

## Running

### Series Calculation in C++

To calculate series and dump them you should run the executable file `main` discussed in the previous section. The program works with the simple following command line (from your build directory):

```
$ ./src/main     series_type    dumper_type    separator    maximum_terms    frequency    precision
```
Arguments are introduced below:
* `series_type` is type of series which can either be `arithmetic` or `pi`.
* `dumper_type` determines how to dump the results and can either be `print` or `write`.
* `separator` is the delimiter which separates numbers in each row in the output file. This argument also determines the extension of the output file. Valid inputs are `tab`, `pipe`, and `comma` corresponding to `.txt`, `.psv`, and `.csv` file extensions respectively. The output file takes the name `series_output` and is generated only when the dumper type is `write`.
* `maximum_terms` is the maximum number of terms included in series calculation. Only positive integers are acceptable here.
* `frequency` is the intermittency of showing the results to the user.  Only positive integers are acceptable here.
* `precision` indicates number of decimal places by which results are shown or written in file. Here also only positive integers are acceptable.

It should be mentioned here that all input arguments are mandatory and should be entered in the correct order. When the program runs, parameters recognized by the program are listed on screen. When the program runs in the dumping mode `print`, results shown on screen are written in a file named `ostream_output.txt` as well. The first and the second columns in results, either on screen or in file, contain number of terms included in series and the value of series summing them, respectively. If analytical value is available, for pi calculation for example, two more columns are shown. In this case, the thrid column shows the analytical (expected) value and the third column shows per cent error of series calculated up to that number of terms.


### Post-Processing with Python

A `output_reader.py` post-processing routine is implemented in the `src` folder. It is intended to plot the series data written to file with the C++ program. This python script is written with `Python 3.7` and requires the libraries `matplotlib`, `numpy` and `argparse` to be installed. The program works with the simple following command line.

```
$ python   output_reader.py   -f   path_to_file   -s   separator
```
The argument `path_to_file` refers to the path to the file containing the data to be plotted (the path is to be taken from the location of the python file). The argument `separator` refers to the type of delimiter used in the data file to separate the different quantities. This second argument can either be `tab`, `comma` or `pipe`.

----

## Work separtion between the authors

The idea was writing mother classes as interfaces first. This task was done at the exercise session: structure of two classes  `Series` and `DumperSeries`, including their virtual functions, was formed and written. To be abale to work remotely, each of us took one of the daughters of  `Series` (namely `compute_arithmetic` and `compute_pi`) and one of the daughters of `DumperSeries` (namely `PrintSeries` and `WriteSeries`) to work on. Moreover, two other major tasks of writing a python script for visualization and modifying `Series` class to avoid re-calculations were split between authors. Each of us developed his own part and worked on his own `main.cc`. Finally, `main`s were merged and the project reviewed.  

----

## Concluding remarks

### Complexity of the program

#### Arithmetic Series

In the arithmetic series, summing from 1 to N needs N adding (+) operations, thus to print or to write the result for different values of N, from 1 to m, number of operations will be:

```
print order     num. of operations
-----------     ------------------
1               1
2               2
3               3
4               4

m               m
----------------------------------
sum1:           m*(m+1)/2
```

However, if we keep track of the latest N as well as the corresponding value of summation, the program needs to only add one term to the available value. So, number of operations will be:
```
print order     num. of operations
-----------     ------------------
1               1
2               1
3               1
4               1

m               1
----------------------------------
sum2:           m
```

Therefore, for a large m the ratio `sum1/sum2` tends to `0.5*m`.

#### Pi Series

For pi calculation, the problem is more complicated. To add each term like `1.0/(i*i)` to the summation, one multiplying operation (*), one inversion operation (/), and one adding operation (+) (totally three operations) are required. Finally, to clculate the approximation for pi, square root of the summation multiplied by 6 is returned, which means two more operations. Hence, number of operations will be:
```
print order     num. of operations
-----------     ------------------
1               1*(3)+2
2               2*(3)+2
3               3*(3)+2
4               4*(3)+2

m               m*(3)+2
----------------------------------
sum2:           m*(3*m+7)/2
```

However, if we keep track of the latest N as well as the corresponding value of summation (without square root), the program needs to only add one term to the available value (three operations), multiply the result by 6, then claculate the square root. So, number of operations will be:
```
print order     num. of operations
-----------     ------------------
1               3+2=5
2               3+2=5
3               3+2=5
4               3+2=5

m               3+2=5
----------------------------------
sum2:           5*m
```

Therefore, for a large m the ratio of `sum1/sum2` tends to `0.3*m`.

----
