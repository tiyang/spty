#ifndef COMPUTE_ARITHMETIC_HH
#define COMPUTE_ARITHMETIC_HH

#include "series.hh"

class compute_arithmetic: public Series
{
public:
    // Functions
    double expression(unsigned int N);  // Expression appeared in the Sigma for arithmetic
    double compute(unsigned int N);     // The target function
};

#endif