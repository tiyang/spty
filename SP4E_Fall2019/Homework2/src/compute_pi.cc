#include "compute_pi.hh"

double compute_pi::expression(unsigned int N)
{
    return 1.0/pow(double(N),2);
}

double compute_pi::compute(unsigned int N)
{
    return sqrt(6.0*SUM(N));
}

double compute_pi::getAnalyticPrediction()
{
    return M_PI;
}








