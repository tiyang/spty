#ifndef COMPUTE_PI_HH
#define COMPUTE_PI_HH

#include "series.hh"

class compute_pi: public Series
{
public:
    // Functions
    double expression(unsigned int N);  // Expression appeared in the Sigma for arithmetic
    double compute(unsigned int N);     // The target function
    double getAnalyticPrediction();     // Analytical expected value: pi number
};

#endif