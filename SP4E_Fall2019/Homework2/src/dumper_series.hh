#ifndef DUMPER_SERIES_HH
#define DUMPER_SERIES_HH

#include "series.hh"
#include <iomanip>
#include <string>
#include <fstream>

class DumperSeries
{
public:
    // Constructor - Destructor
    DumperSeries(Series& series): series(series), dump_precision(4){}
    virtual ~DumperSeries(){}

    // Functions
    virtual void dump(std::ostream& os = std::cout) = 0;    // The function that finally dumps numbers
    void setPrecision(unsigned int precision);  // Setter function for dump_precision

protected:
    // Members
    Series& series;                             // Series to be dumped
    unsigned int dump_precision;                // Number of decimal places in writing either on screen or in file
};

inline std::ostream& operator << (std::ostream & stream, DumperSeries & _this)
{
    _this.dump(stream);
    return stream;
}

#endif