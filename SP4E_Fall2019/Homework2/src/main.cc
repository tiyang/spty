#include "series.hh"
#include "compute_arithmetic.hh"
#include "compute_pi.hh"
#include "dumper_series.hh"
#include "print_series.hh"
#include "write_series.hh"
#include <sstream>

using namespace std;

// Functions declaration
void decide_series(Series *&new_series, string type);
void decide_dumper(DumperSeries *&new_dumper, Series *&ptrSeries, string type, int Nmbr, int frqncy, string sprtr, int prcsn);

int main(int argc, char ** argv)
{
    int N_inputs = argc;
    std::stringstream inputs;
    for (int i = 1; i < argc; ++i)
    {
        inputs << argv[i] << " ";
    }

    string serie_type;          // Type of series (Arithmetic or Pi)
    string dump_type;           // Type of dumping the result
    string separator;           // Sepearator of numbers in files
    unsigned long N;            // Maximum number of calculations of series
    unsigned long freq;         // Frequency for dumping series
    unsigned long precision;    // Precision of writing or printing numbers

    inputs >> serie_type;
    inputs >> dump_type;
    inputs >> separator;
    inputs >> N;
    inputs >> freq;
    inputs >> precision;

    if (N<1)
    {
        std::cout<<"ERROR: Invalid input. N must be a positive integer (N>0)"<<std::endl;
        exit(0);
    }

    // Summarizing inputs on screen
    printf("###### User Inputs ###### \n");
    cout << "Serie type: "     << serie_type   << endl;
    cout << "Dump type: "      << dump_type    << endl;
    cout << "Separator type: " << separator    << endl;
    cout << "Max iterations: " << N            << endl;
    cout << "Dump frequency: " << freq         << endl;
    cout << "Precision: "      << precision    << endl;
    printf("######################### \n");

    // Pointers
    Series       *ptrSeries = nullptr;  // Pointer to Series
    DumperSeries *ptrDumper = nullptr;  // Pointer to Dumper

    // Choose series to be implemented
    decide_series(ptrSeries, serie_type);
    decide_dumper(ptrDumper, ptrSeries, dump_type, N, freq, separator, precision);

    // Do the calculation of series
    ptrDumper -> dump();

    if (dump_type == "print")
    {
        std::ofstream my_os_file("ostream_output.txt", std::ios::out | std::ios::trunc);
        ptrDumper -> dump(my_os_file);
        my_os_file.close();
    }

    // Free up memories
    delete ptrSeries;
    delete ptrDumper;

    return 0;
}


void decide_series(Series *&new_series, string type)
{
    if (type == "arithmetic")
    {
        new_series = new compute_arithmetic();
    }
    else if (type == "pi")
    {
        new_series = new compute_pi();
    }
    else
    {
        cout<<"ERROR: Invalid series type. Please enter 'arithmetic' or 'pi'."<<endl;
        exit(0);
    }
}

void decide_dumper(DumperSeries *&new_dumper, Series *&ptrSeries, string type, int Nmbr, int frqncy, string sprtr, int prcsn)
{
    if (type == "print")
    {
        new_dumper = new PrintSeries(*ptrSeries, Nmbr, frqncy, prcsn);
    }
    else if (type == "write")
    {
        new_dumper = new WriteSeries(*ptrSeries, Nmbr, sprtr, prcsn);
    }
    else
    {
        cout<<"ERROR: Invalid dumbper type. Please enter 'print' or 'write'."<<endl;
        exit(0);
    }
}