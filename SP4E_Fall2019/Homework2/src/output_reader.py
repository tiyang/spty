import numpy as np
import argparse
import matplotlib.pyplot as plt

# Arguments parser
parser = argparse.ArgumentParser(description='Series visualization')
parser.add_argument('-f', '--filename', type=str, help=('path to the file to be visualized'),required=True)
parser.add_argument('-s', '--separator', type=str, help=('data separator type'),choices=['tab', 'comma', 'pipe'], required=True)

args = parser.parse_args()
filename = args.filename
separator = args.separator

if separator == 'tab':
    delimiter = '\t'
elif separator == 'comma':
    delimiter = ', '
elif separator == 'pipe':
    delimiter = '|'
else:
    print('Unknown delimiter -> abort')
    quit()
        
# Load data
data = np.loadtxt(filename, delimiter = delimiter)

N_parameters = len(data[0,:])

# Post-processing
if N_parameters == 4:
    steps = data[:,0]
    result = data[:,1]
    analytical = data[:,2]
    error = data[:,3]
    
    N_iteration = len(steps)
    
    fig = plt.figure(1)
    ax = fig.add_subplot(111)
    ax.plot(steps, result, 'ko')
    ax.plot(steps, analytical, 'r-')
    ax.set_xlabel('Iterations [-]')
    ax.set_ylabel('Pi value [-]')
    plt.legend(('Pi computed value', 'Pi analytical value'), loc = 'center right')
    ax.set_title('Pi Series with %s iterations' % N_iteration)
    
    ax2 = ax.twinx()
    color = 'tab:blue'
    ax2.plot(steps, error, 'bs', color = color)
    ax2.set_ylabel('Realtive error [%]', color = color)
    ax2.tick_params(axis = 'y' ,labelcolor = color)
    plt.show()
    
    
elif N_parameters == 2:
    steps = data[:,0]
    result = data[:,1]
    
    N_iteration = len(steps)
    
    fig = plt.figure(1)
    ax = fig.add_subplot(111)
    ax.plot(steps, result, 'ks')
    ax.set_xlabel('Iterations [-]')
    ax.set_ylabel('Sum value [-]')
    ax.set_title('Arithmetic Series with %s iterations' % N_iteration)
    plt.show()