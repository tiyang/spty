#include "print_series.hh"

PrintSeries::PrintSeries(Series & series, unsigned int N, unsigned int freq, unsigned int precision): DumperSeries(series)
{
    maxiter = N;
    frequency = freq;
    setPrecision(precision);
}

void PrintSeries::dump(std::ostream& os)
{
    double computed, analytical, error;

    for (int i=1; i <= maxiter; i++)
    {
        if ((i-1) % frequency == 0)
        {
            computed = series.compute(i);

            os << i << "\t" << std::fixed << std::setprecision(dump_precision) << computed;

            // Checking whether analytical value exists
            if (!std::isnan(series.getAnalyticPrediction()))
            {
                analytical = series.getAnalyticPrediction();
                error = 100.0 * std::abs(analytical - computed) / analytical;
                os << "\t" <<  std::fixed << std::setprecision(dump_precision) << analytical << "\t" << error <<" %";
            }

            os << std::endl;
        }
    }
}