#ifndef PRINT_SERIES_HH
#define PRINT_SERIES_HH

#include "dumper_series.hh"

class PrintSeries: public DumperSeries
{
public:
    // Constructor
    PrintSeries(Series& series, unsigned int N, unsigned int freq, unsigned int precision = 4);

    // Functions
    void dump(std::ostream& os = std::cout);    // Definition of the dump for PRINT defined virtually in the mother class

private:
    // Members
    unsigned int maxiter;                       // Maximum number of calculating series to see it convergence
    unsigned int frequency;                     // Period of writing/printing the results
};

#endif