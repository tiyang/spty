#include "series.hh"

double Series::SUM(unsigned int N)
{
    while (current_index != N)
    {
        if (N>current_index)
        {
            current_index += 1;
            current_value += expression(current_index);
        }
        else
        {
            current_value -= expression(current_index);
            current_index -= 1;
        }
    }
    return current_value;
}

double Series::getAnalyticPrediction()
{
    return std::nan("");
}