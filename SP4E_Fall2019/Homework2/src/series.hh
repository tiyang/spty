#ifndef SERIES_HH
#define SERIES_HH

#include <iostream>
#include <cmath>

class Series
{
public:
    // Constructor - Destructor
    Series(): current_index(0), current_value(0.0){}
    virtual ~Series(){};

    // Functions
    virtual double expression(unsigned int N) = 0;  // The expression appeared in Sigma
    virtual double compute(unsigned int N) = 0;     // Computes the solution as a function of summation
    virtual double getAnalyticPrediction();         // Returns analytical asymptotic solution of compute (NaN by default)
    double SUM(unsigned int N);                     // Sums expression from 1 to N

    // Members
    unsigned int current_index;                     // Upper bound of the last summation
    double       current_value;                     // Value of the last summation
};

#endif