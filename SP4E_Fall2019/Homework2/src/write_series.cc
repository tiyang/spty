#include "write_series.hh"

WriteSeries::WriteSeries(Series& series, unsigned int N, std::string separator, unsigned int precision): DumperSeries(series)
{
    maxiter = N;
    dump_precision = precision;
    setSeparator(separator);
}

void WriteSeries::setSeparator(std::string separator)
{
    if (separator == "comma")
    {
        sep     = ", ";
        format  = ".csv";
    }
    else if (separator == "pipe")
    {
        sep     = "|";
        format  = ".psv";
    }
    else
    {
        sep     = "\t";
        format  = ".txt";

        if (separator != "tab")
        {
            std::cout << "----> WARNING: Data are written to a txt fille with a tab separator." << std::endl;
        }
    }
}

void WriteSeries::dump(std::ostream & os)
{
    std::string file_name = "series_output";
    file_name.append(format);

    std::ofstream my_file;
    my_file.open(file_name, std::ios::out | std::ios::trunc);

    double computed, analytical, error;

    for (int i = 1; i <= maxiter; i++)
    {
        computed = series.compute(i);
        my_file << i << sep << std::fixed << std::setprecision(dump_precision) << computed;

        // Checking whether analytical value exists
        if (!std::isnan(series.getAnalyticPrediction()))
        {
            analytical = series.getAnalyticPrediction();
            error = 100.0 * std::abs(analytical - computed) / analytical;
            my_file << sep << std::fixed << std::setprecision(dump_precision) << analytical
                    << sep << std::fixed << std::setprecision(dump_precision) << error;
        }

        my_file << std::endl;
    }

    my_file.close();
}