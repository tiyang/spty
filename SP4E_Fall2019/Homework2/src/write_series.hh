#ifndef WRITE_SERIES_HH
#define WRITE_SERIES_HH

#include "dumper_series.hh"

class WriteSeries: public DumperSeries
{
public:
    // Constructor
    WriteSeries(Series& series, unsigned int N, std::string separator = "tab", unsigned int precision = 4);

    // Functions
    void setSeparator(std::string separator);   // Setter function for the separator and file type
    void dump(std::ostream& os = std::cout);    // Definition of the dump for WRITE defined virtually in the mother class

private:
    // Members
    unsigned int maxiter;                       // Upper bound of summation
    std::string sep;                            // Separator of numbers in file
    std::string format;                         // Output file format
};

#endif