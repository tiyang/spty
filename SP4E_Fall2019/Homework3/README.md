
# SP4E - Homework 3

----

## General Info

This file provides a brief documentation and information related to the third Homework of the course "Scientific Programming for Engineers", fall 2019.

This homework is done by **O. Ashtari** and **A. Sieber**.

Last update: 12.11.2019

----

## Project Description

One goal of this project is to learn and practice how to link an external library, here FFTW, to a C++ code. We use fast Fourier transforms to solve heat equation on a 2D square domain using a pseudo-spectral method. For this aim, as the second objective of the project, we extend the existing code originally written for simulating dynamics of interacting particles and the displacement of planets. Many functionalities we need have been already implemented and tested in the available code. We will therefore use those functionalities as a starting point to our project.

----

## Executable Files

After cloning this repository on your computer, you should build the program in a desired directory based on the `CMakeLists.txt` file in the root folder. To do so, from within the destination directory, you can build the executable file(s) using command line below:
```
$ cmake -DCMAKE_PREFIX_PATH=<include> -DCMAKE_LIBRARY_PATH=<library> -DUSE_FFT=<status> <CMakeLists>
$ make
```
where the command `cmake` is basically followed by the address of the directory where the `CMakeLists.txt` is located (denoted by `<CMakeLists>`.) Additional options are available as follows:
* `-DCMAKE_PREFIX_PATH` specifies the path where you want the program to look for headers (.h files). `<include>` in the command above will be replaced by the address.
* `-DCMAKE_LIBRARY_PATH` specifies the path where you want the program to look for libraries (.a files). `<library>` in the command above will be replaced by the address.
* `-DUSE_FFT` determines whether the program should look for `FFTW` library/header in the specified directories or not. `<status>` can either be `ON` or `OFF`.

Note that `googletest` must be installed in the `lib` folder to build the executables. The library can be cloned from `github.com`.

The executable file `particles` will be built in `<build_directory>/src`.  Moreover, three tests `test_fft`, `test_heat` and `test_kepler` are built. The minimum requirement for CMake is the version 3.1.


----

## Running a simulation: from generating initial condition to post-processing

### 1- Generating initial condition

The first step in running a simulation consist in setting up the initial conditions. This is done through the python script `input_generator.py` located in the `tools` folder. By running this code, the user is asked to chose the following options:
* Simulation test case `-simulation`, by default `line_source`.
* The grid size `-N_grid`, by default 32. Nothe that FFTW is much more efficient if  `-N_grid` is a power of 2.
* The half domain extend `-L` in both the x- and y-directions, by default 1. Don't forget to use 2L as length of the domain when running the C++ program for integrating in time.
* The uniform initial temperature `-T`, by default 0. Since only derivatives of temperature exist in the governing equation, a shift in temperature does not make any difference in the dynamics of the problem. That's why when you use dirichlet boundary conditions, temperature at boundaries is set equal to 0. So we do not suggest the user to change the value of the uniform temperature.
* The radius of the heat source `-R`, by default 0.5. This argument is only used if `radial_source` is chosen as the simulation type.

The available simulation test cases (for the flag `-simulation`) are the following:
* Uniform temperature without heat source, `uniform_temperature`.
* Uniform temperature with line heat sources, `line_source`.
* Uniform temperature with radial heat source, `radial_source`.

As an example for each type of simulation, see the following command lines:
```
$ python3   -simulation uniform_temperature   -N_grid 512   -L 1.0   -T 1.0
$ python3   -simulation line_source           -N_grid 512   -L 1.0   -T 0.0
$ python3   -simulation radial_source         -N_grid 512   -L 1.0   -T 0.0   -R 0.5
```

which initialized the desired condition on a [-1;1]x[-1;1] domain and a 512x512 grid.

### 2- Running the calculations

Once this step is achieved, the actual simulation can be performed. This is done by running the `particles` executable through the command line alongside a set a user-defined variables.

```
$ <build directory>/src/particles <n_steps> <dump_freq> <input_file> <particles_type> <time_step> <boundary_type> <domain_length> <density*heat_capacity> <heat_conductivity>
```

The hereinabove arguments represent:
* `n_steps`, the number of time steps. Must be an integer.
* `dump_freq`, the output files dumping frequency. Must be an integer.
* `input_file`, the path to the input file generated with the `input_generator.py` script.
* `particles_types`, the type of simulation. Here, for heat equation, the string `material_point` must be used.
* `time_step`, the integration time step size. Must be a double.
* `boundary_type`, the boundary type. As of now, only `Dirichlet` or `Periodic` boundary conditions are implemented.
* `domain_length`, the TOTAL length of the domain in both the x- and y-direction. To be consistent with the `L` used in the python script, here the `domain_length` must be set to 2xL.
* `density*heat_capacity`, the product of the mass density and the heat capacity of the simulated material.
* `heat_conductivity`, the heat conductivity of the simulated material.

The user is also asked to create a `dumps` folder where the dumped file can be stored. An example of the run command line is given below:

```
$ ./particles 5000 10 input.csv material_point 0.0005 Dirichlet 2 1 0.5
```
This simulation consists of 5000 time steps of 0.0005 on a [-1;1]x[-1;1] domain. Outputs are dumped every 10 time steps. Dirichlet boundary conditions are implemented at the boundaries. The product of the mass density and the heat capacity is 1 and the heat conductivity is 0.5. Every other simulation properties are set in `input_generator.py` script.
### 3- Visualization
The visualization of the results can be done with the `Paraview` software. Here, we introduce two different ways to visualize the results in this software: (1) through a simple step-by-step guide and (2) with a macro that contains all steps in a Python `.py` file.

#### Step-by-step instruction
Step 1: First, the dumped files are loaded into the software (by selecting `step-..csv` group of output files inside the `dumps` folder.) After loading the files, be sure to set the `field Delimiter Character` to a simple space, and uncheck the `Have Headers` box. Then click on `Apply`.

Step 2: The `Table To Points` filter allows to convert the data points to coordinates. The x-coordinates correspond to the `field 0`, the y-coordinates correspond to the `field 1` and the z-coordinates correspond to the `field 2`. Click on `Apply` after making these changes.

Step 3: Finally, to visualization different fields add a proper filter like `Delaunay 2D` or `Point Volume Interpolator` to the `Table To Points` object from step 2. Visualization of the temperature field is achieved by setting the `Coloring` option of the `field 13` whereas the `field 14` shows the heat source distribution. Finally, in the Properties box under Coloring, click on  `Rescale to data range over all timesteps` and enjoy the animation.

#### Using the provided macro
A file named `Visualization.py` is located in the `tools` folder. To use this macro, under the tab `Macros` click on `Add new macro...`, then select the `.py` file. Now if you click on `Macros` you see that a new row, named `Visualization`, is added to the menu. Having the macro available you need to open a group of results (`<running directory>/dumps/step-..csv`) and then click on the recently added item `Visualization` (under `Macros`.) Steps described in the previous subsection will be taken. Finally, under Coloring option from the Properties box, click on  `Rescale to data range over all timesteps` and enjoy the animation.


## Integration of the boundary condition in the existing code

The boundary conditions for heat equation are handled in the `heat_boundary.hh` and `heat_boundary.cc` source files defining the class  `Heat_Boundary`.  The class `ComputeTemperature` (defined in `compute_temperature.hh` file) has a member of type `Heat_Boundary`.

The process works as follow: From the command line, the code decides whether the `BC_Type`, member of the class `Heat_Boundary`, should be `Dirichlet` or `Periodic` (see `material_points_factory`.) Then at each time step, all nodes are first updated without considering the boundary conditions, then `heat_boundary.hh`'s `compute` method is called and imposes the proper boundary conditions, based on `BC_Type` on nodes that are located on boundaries, i.e. nodes that correspond to `i=0` or `j=0` when looping over `i` and `j` to swipe all the grid.
