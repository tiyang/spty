#ifndef __COMPUTE_TEMPERATURE__HH__
#define __COMPUTE_TEMPERATURE__HH__

/* -------------------------------------------------------------------------- */
#include "compute.hh"
#include "heat_boundary.hh"

//! Integrate heat equation
class ComputeTemperature : public Compute {

    // Constructors/Destructors
public:
    ComputeTemperature(Real timestep,
                       Real Length,
                       Real rho_C,
                       Real kappa,
                       std::string BC_type);

  // Methods
public:
    //! Set time step
    void setDeltaT(Real dt);
    //! Evolve temperature
    void compute(System& system) override;
    
private:
    Real dt;            // Time step size
    Real L;             // Length of the domain in x and y
    Real rhoC;          // (density)*(heat capacity)
    Real k;             // Conductivity
    Heat_Boundary BC;   // Boundary condition
    
};

/* -------------------------------------------------------------------------- */
#endif  //__COMPUTE_TEMPERATURE__HH__
