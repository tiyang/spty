#ifndef FFT_HH
#define FFT_HH
/* ------------------------------------------------------ */
#include "matrix.hh"
#include "my_types.hh"
#include "fftw3.h"
/* ------------------------------------------------------ */

struct FFT {

  static Matrix<complex> transform(Matrix<complex>& m);
  static Matrix<complex> itransform(Matrix<complex>& m);

  static Matrix<std::complex<int>> computeFrequencies(int size);

};

/* ------------------------------------------------------ */

inline Matrix<complex> FFT::transform(Matrix<complex>& m_in) {
    fftw_complex *C_in;                     // Input physical domain as a rowmajor vector
    fftw_complex *C_out;                    // Output spectral domain as a rowmajor vector
    fftw_plan forward_plan;                 // FFT plan
    UInt length = std::pow(m_in.size(),2);  // Length of vectors
    Matrix<complex> m_out(m_in.size());     // The output matrix
    
    // Forming rowmajor vectors
    C_in = new fftw_complex [length];
    C_out = new fftw_complex [length];
    
    // Filling the input vector
    for (auto&& entry : index(m_in))
    {
        int i = std::get<0>(entry);
        int j = std::get<1>(entry);
        auto& val = std::get<2>(entry);
        C_in[i*m_in.size()+j][0]=val.real();
        C_in[i*m_in.size()+j][1]=0.0;
    }

    // Executing the plan
    forward_plan =  fftw_plan_dft_2d(m_in.size(), m_in.size(), C_in, C_out, FFTW_FORWARD, FFTW_ESTIMATE);
    fftw_execute(forward_plan);
    
    for (auto&& entry : index(m_out))
    {
        int i = std::get<0>(entry);
        int j = std::get<1>(entry);
        auto& val = std::get<2>(entry);
        val=complex(C_out[i*m_in.size()+j][0], C_out[i*m_in.size()+j][1])/double(std::pow(m_in.size(),2));
    }
    
    // Clean up
    fftw_destroy_plan(forward_plan);
    fftw_cleanup();
    fftw_free(C_in);
    fftw_free(C_out);
    
    return m_out;
}

/* ------------------------------------------------------ */

inline Matrix<complex> FFT::itransform(Matrix<complex>& m_in) {
    fftw_complex *C_in;                     // Input spectral domain as a rowmajor vector
    fftw_complex *C_out;                    // Output physical domain as a rowmajor vector
    fftw_plan backward_plan;                // FFT plan
    UInt length = std::pow(m_in.size(),2);  // Length of vectors
    Matrix<complex> m_out(m_in.size());     // The output matrix

    // Forming rowmajor vectors
    C_in = new fftw_complex [length];
    C_out = new fftw_complex [length];

    // Filling the input vector
    for (auto&& entry : index(m_in))
    {
        int i = std::get<0>(entry);
        int j = std::get<1>(entry);
        auto& val = std::get<2>(entry);
        C_in[i*m_in.size()+j][0]=val.real();
        C_in[i*m_in.size()+j][1]=val.imag();
    }

    // Executing the plan
    backward_plan = fftw_plan_dft_2d(m_in.size(), m_in.size(), C_in, C_out, FFTW_BACKWARD, FFTW_ESTIMATE);
    fftw_execute(backward_plan);

    for (auto&& entry : index(m_out))
    {
        int i = std::get<0>(entry);
        int j = std::get<1>(entry);
        auto& val = std::get<2>(entry);
        val=complex(C_out[i*m_in.size()+j][0], 0.0);    // Output is real valued function.
    }

    // Clean up
    fftw_destroy_plan(backward_plan);
    fftw_cleanup();
    fftw_free(C_in);
    fftw_free(C_out);

    return m_out;
}

/* ------------------------------------------------------ */

inline Matrix<std::complex<int>> FFT::computeFrequencies(int size) {
    // Note: Be careful that later in calculating wave vectors
    // you should consider the length of the phisical domain.
    // Here, the real part stores wave number for y direction (i)
    // and the imaginary part stores wave nomber for x direction (j)
    // Note: Number of grids in each direction should be a power of 2
    
    Matrix<std::complex<int>> Wave_number(size);
    
    for (auto&& entry : index(Wave_number))
    {
        int i = std::get<0>(entry);
        int j = std::get<1>(entry);
        auto& val = std::get<2>(entry);
        
        i<size/2 ? val.real(i) : val.real(i-size);
        j<size/2 ? val.imag(j) : val.imag(j-size);
    }
    
    return Wave_number;
}
/* ------------------------------------------------------ */

#endif  // FFT_HH
