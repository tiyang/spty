#include "heat_boundary.hh"

/* -------------------------------------------------------------------------- */

void Heat_Boundary::set_BC(std::string BC)
{
    BC_Type = BC;
    
    if(BC == "Dirichlet")
        std::cout<<"Dirichlet boundary condition set for all boundaries."<<std::endl;
    else
        std::cout<<"Periodic boundary condition set in both x- and y- direction."<<std::endl;
}

void Heat_Boundary::compute(System& system) {
    UInt nb_particles = system.getNbParticles();    // Number of particles (grids)
    UInt size = std::sqrt(nb_particles);            // Discretization in each side
    UInt id;                                        // Particle id to call

    if (BC_Type == "Dirichlet"){
        for(int i=0; i<size; i++){
            // id = i + j*size   &   j=0
            id = i;
            auto& prtcl = dynamic_cast<MaterialPoint&>(system.getParticle(id));
            prtcl.getTemperature() = 0.0;
        }
        
        for(int j=0; j<size; j++){
            // id = i + j*size   &   i=0
            id = j*size;
            auto& prtcl = dynamic_cast<MaterialPoint&>(system.getParticle(id));
            prtcl.getTemperature() = 0.0;
        }
    }
}

/* -------------------------------------------------------------------------- */
