#ifndef __HEAT_BOUNDARY__HH__
#define __HEAT_BOUNDARY__HH__

/* -------------------------------------------------------------------------- */
#include "compute.hh"
#include "material_point.hh"
/* -------------------------------------------------------------------------- */

//! Compute interaction with simulation box
class Heat_Boundary : public Compute{
    public:
      void set_BC(std::string BC);
      void compute(System& system);
        
    private:
        std::string BC_Type;
};

/* -------------------------------------------------------------------------- */
#endif  //__HEAT_BOUNDARY__HH__
