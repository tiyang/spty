#include "compute_gravity.hh"
#include "compute_verlet_integration.hh"
#include "csv_reader.hh"
#include "csv_writer.hh"
#include "my_types.hh"
#include "ping_pong_balls_factory.hh"
#include "material_points_factory.hh"
#include "planets_factory.hh"
#include "system.hh"
/* -------------------------------------------------------------------------- */
#include <cstdlib>
#include <iostream>
#include <sstream>
/* -------------------------------------------------------------------------- */

// ----------------- Functions declaration -----------------
void help();                            // Printing help and exit
void argnum_error(int argc, int n);     // Checking number of extra arguments

// ----------------- main program -----------------
int main(int argc, char** argv) {
  if (argc < 6)
      help();

  Real nsteps;                              // the number of steps to perform
  std::stringstream(argv[1]) >> nsteps;
  int freq;                                 // freq to dump
  std::stringstream(argv[2]) >> freq;
  std::string filename = argv[3];           // init file
  std::string type = argv[4];               // particle type
  Real timestep;                            // time step size
  std::stringstream(argv[5]) >> timestep;

  if (type == "planet")
    PlanetsFactory::getInstance();
  else if (type == "ping_pong")
    PingPongBallsFactory::getInstance();
  else if (type == "material_point"){
      argnum_error(argc, 10);
      MaterialPointsFactory::getInstance();
  }
  else {
    std::cout << "Unknown particle type: " << type << std::endl;
    std::exit(EXIT_FAILURE);
  }

  ParticlesFactoryInterface& factory = ParticlesFactoryInterface::getInstance();

  SystemEvolution& evol = factory.createSimulation(filename, timestep, argc, argv);

  evol.setNSteps(nsteps);
  evol.setDumpFreq(freq);

  evol.evolve();

  return EXIT_SUCCESS;
}

// ----------------- Functions definition -----------------
void help()
{
    std::cout<< "\n===============================================\n"<<
                "To use this program, the following arguments\n"<<
                "must be enterd in correct order:\n\n"<<
                "ORDER      TYPE           ITEM\n"<<
                "1          <int>          num of steps\n"<<
                "2          <int>          dump frequency\n"<<
                "3          <string>       input file name\n"<<
                "4          <string>       particle type\n"<<
                "5          <double>       time step size\n"<<
                "6          <string>       boundary type\n"<<
                "7          <double>       length of the domain\n"<<
                "8          <double>       density*heat_capacity\n"<<
                "9          <double>       heat conductivity\n\n"<<

                "* Particle type can be:\n"<<
                "  'planet', 'ping_pong', or 'material_point'.\n\n"<<
                "* Boundary type can be:\n"<<
                "  'Dirichlet' or 'Periodic'.\n\n"<<
                "* Items 6-9 are applicable for the case\n"<<
                "  of material_point particle type."<<
                "\n===============================================\n\n";
    
    std::exit(EXIT_FAILURE);
}

void argnum_error(int argc, int n)
{
    if (argc != n){
        std::cout<< "\n\n>>> ERROR: number of arguments does not\n"<<
                    ">>>        match the type of particles.\n";
        help();
    }
}
