#include <fstream>
#include <stdio.h>
#include <gtest/gtest.h>
#include <iomanip>
#include "material_points_factory.hh"

/* ------------------------------------------------------ */

// Simulation parameters
int argc = 10;  // Number of arguments
Real L;         // Length of the domain
Real nsteps;    // the number of steps to perform
Real timestep;  // timestep

/* ------------------------------------------------------ */

TEST(HEAT, uniform1) {
    /* We expect that in the case of h=0 and
     * uniform temperature distribution, the temperature
     * domain stays constant for all time. */
    
    // Temporal command line
    char *argv[] = {NULL,
        (char*)"100",
        (char*)"0",
        (char*)"initial.csv",
        (char*)"material_point",
        (char*)"0.1",
        (char*)"Periodic",
        (char*)"2.0",
        (char*)"1.0",
        (char*)"0.5"
    };
    
    std::stringstream(argv[7]) >> L;
    
    // Constructing the file with uniform temp and zero h
    int size=16;
    std::ofstream fout("initial.csv");
    fout<<std::setprecision(16);
    for(int j=0; j<size; j++){
        for(int i=0; i<size; i++){
            fout<<L*((double(j)/double(size))-0.5)<<" "
                <<L*((double(i)/double(size))-0.5)<<" "
                <<"0 0 0 0 0 0 0 0 1.0 0"<<std::endl;;
        }
    }
    fout.close();
    
    std::stringstream(argv[1]) >> nsteps;
    std::stringstream(argv[5]) >> timestep;
    
    MaterialPointsFactory::getInstance();
    ParticlesFactoryInterface& factory = ParticlesFactoryInterface::getInstance();
    
    SystemEvolution& evol = factory.createSimulation("initial.csv", timestep, argc, argv);
    evol.setNSteps(nsteps);

    // Evolution in time, without dumping
    for (UInt i = 0; i < nsteps; i++){
        evol.getCompute(0).compute(evol.getSystem());
    }
    
    // Testing equality
    int id;
    for(UInt j=0; j<size; j++){
        for(UInt i=0; i<size; i++){
            id = j+i*size;
            auto& ip = dynamic_cast<MaterialPoint&>(evol.getSystem().getParticle(id));
            ASSERT_EQ(ip.getTemperature(), 1.0);
        }
    }
    
    remove("initial.csv");
}

TEST(HEAT, uniform2) {
    /* We expect that in the case of h=0, any temperature
     * distribution will decay to the average of the
     * initial domain when time tends to infinity.
     * Here, sin(4*pi*x/L) must decay to zero. */
    
    // Temporal command line
    char *argv[] = {NULL,
        (char*)"500",
        (char*)"0",
        (char*)"initial.csv",
        (char*)"material_point",
        (char*)"0.0125",
        (char*)"Periodic",
        (char*)"2.0",
        (char*)"1.0",
        (char*)"1.0"
    };
    
    std::stringstream(argv[7]) >> L;
    
    // Constructing the file with uniform temp and zero h
    int size=8;
    std::ofstream fout("initial.csv");
    fout<<std::setprecision(16);
    for(int j=0; j<size; j++){
        for(int i=0; i<size; i++){
            fout<<L*((double(j)/double(size))-0.5)<<" "
                <<L*((double(i)/double(size))-0.5)<<" "
                <<"0 0 0 0 0 0 0 0 "
                <<sin(2.0*M_PI*double(i)/double(size))
                <<" 0"<<std::endl;;
        }
    }
    fout.close();
    
    std::stringstream(argv[1]) >> nsteps;
    std::stringstream(argv[5]) >> timestep;
    
    MaterialPointsFactory::getInstance();
    ParticlesFactoryInterface& factory = ParticlesFactoryInterface::getInstance();
    
    SystemEvolution& evol = factory.createSimulation("initial.csv", timestep, argc, argv);
    evol.setNSteps(nsteps);

    // Evolution in time, without dumping
    for (UInt i = 0; i < nsteps; i++){
        evol.getCompute(0).compute(evol.getSystem());
    }
    
    // Testing equality
    int id;
    for(UInt j=0; j<size; j++){
        for(UInt i=0; i<size; i++){
            id = j+i*size;
            auto& ip = dynamic_cast<MaterialPoint&>(evol.getSystem().getParticle(id));
            ASSERT_NEAR(ip.getTemperature(), 0, 1e-11);
        }
    }
    
    remove("initial.csv");
}

TEST(HEAT, line_source) {
      /* We expect that in the case of line heat sink and sorces,
       * h=-1 at x=-0.5 and h=+1 at x=+0.5,
       * steady temperature distribution below forms as the
       * steady state solution as time tends to infinity.
       * T=-x-1     if -1   < x < -0.5
       * T=x        if -0.5 < x < +0.5
       * T=-x+1     if +0.5 < x < +1
       * The domain is 2x2 with center on the origin.
       * We start the simulation from zero distribution. */
      
      // Temporal command line
      char *argv[] = {NULL,
          (char*)"100",
          (char*)"0",
          (char*)"initial.csv",
          (char*)"material_point",
          (char*)"0.0001",
          (char*)"Periodic",
          (char*)"2.0",
          (char*)"1.0",
          (char*)"0.5"
      };
      
      std::stringstream(argv[7]) >> L;
      
      // Constructing the file with uniform temp and zero h
      int size=128;
      double dx = L / double(size);         // The impulse should be 1/dx
    
      std::ofstream fout("initial.csv");
      fout<<std::setprecision(16);
      for(int j=0; j<size; j++){
          for(int i=0; i<size; i++){
              Real x = L*((double(j)/double(size))-0.5);
              Real y = L*((double(i)/double(size))-0.5);
              fout  << x <<" "
                    << y <<" "
                    << "0 0 0 0 0 0 0 0 ";
              
              if (j <= size/4)
                  fout << -x - 1.0;
              else if (j>size/4 && j<3*size/4)
                  fout << x;
              else
                  fout << -x + 1.0;
              
              fout<<" "<<(-double(j==(size/4))+double(j==(3*size/4)))/dx<< std::endl;;
          }
      }
      fout.close();
      
      std::stringstream(argv[1]) >> nsteps;
      std::stringstream(argv[5]) >> timestep;
      
      MaterialPointsFactory::getInstance();
      ParticlesFactoryInterface& factory = ParticlesFactoryInterface::getInstance();
      
      SystemEvolution& evol = factory.createSimulation("initial.csv", timestep, argc, argv);
      evol.setNSteps(nsteps);

      // Evolution in time, without dumping
      for (UInt i = 0; i < nsteps; i++){
          evol.getCompute(0).compute(evol.getSystem());
      }
    
      // Testing equality
      UInt id;
      for(UInt j=0; j<size; j++){
          for(UInt i=0; i<size; i++){
              id = i+j*size;
              auto& ip = dynamic_cast<MaterialPoint&>(evol.getSystem().getParticle(id));

              if (j < size/4)
                  ASSERT_NEAR(ip.getTemperature(),
                              -ip.getPosition()[0]-1.0,
                              1e-3);
              if (j>size/4 && j<3*size/4)
                  ASSERT_NEAR(ip.getTemperature(),
                              ip.getPosition()[0],
                              1e-3);
              if (j > 3*size/4)
                   ASSERT_NEAR(ip.getTemperature(),
                               -ip.getPosition()[0]+1.0,
                               1e-3);
          }
      }
      
      remove("initial.csv");
}
