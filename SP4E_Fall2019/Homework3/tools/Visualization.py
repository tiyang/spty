# trace generated using paraview version 5.7.0-RC3

#### import the simple module from the paraview
from paraview.simple import *
#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

# get active source.
step00 = GetActiveSource()

# Properties modified on step00
step00.HaveHeaders = 0
step00.FieldDelimiterCharacters = ' '

# Create a new 'SpreadSheet View'
spreadSheetView1 = CreateView('SpreadSheetView')
spreadSheetView1.ColumnToSort = ''
spreadSheetView1.BlockSize = 1024L
# uncomment following to set a specific view size
# spreadSheetView1.ViewSize = [400, 400]

# show data in view
step00Display = Show(step00, spreadSheetView1)

# get layout
layout1 = GetLayoutByName("Layout #1")

# add view to a layout so it's visible in UI
AssignViewToLayout(view=spreadSheetView1, layout=layout1, hint=0)

# destroy spreadSheetView1
Delete(spreadSheetView1)
del spreadSheetView1

# close an empty frame
layout1.Collapse(2)

# find view
renderView1 = FindViewOrCreate('RenderView1', viewtype='RenderView')
# uncomment following to set a specific view size
# renderView1.ViewSize = [2150, 1142]

# set active view
SetActiveView(renderView1)

# create a new 'Table To Points'
tableToPoints1 = TableToPoints(Input=step00)
tableToPoints1.XColumn = 'Field 0'
tableToPoints1.YColumn = 'Field 0'
tableToPoints1.ZColumn = 'Field 0'

# Properties modified on tableToPoints1
tableToPoints1.YColumn = 'Field 1'
tableToPoints1.ZColumn = 'Field 2'
tableToPoints1.a2DPoints = 1
tableToPoints1.KeepAllDataArrays = 1

# show data in view
tableToPoints1Display = Show(tableToPoints1, renderView1)

# trace defaults for the display properties.
tableToPoints1Display.Representation = 'Surface'
tableToPoints1Display.ColorArrayName = [None, '']
tableToPoints1Display.OSPRayScaleArray = 'Field 0'
tableToPoints1Display.OSPRayScaleFunction = 'PiecewiseFunction'
tableToPoints1Display.SelectOrientationVectors = 'Field 0'
tableToPoints1Display.ScaleFactor = 0.2
tableToPoints1Display.SelectScaleArray = 'Field 0'
tableToPoints1Display.GlyphType = 'Arrow'
tableToPoints1Display.GlyphTableIndexArray = 'Field 0'
tableToPoints1Display.GaussianRadius = 0.01
tableToPoints1Display.SetScaleArray = ['POINTS', 'Field 0']
tableToPoints1Display.ScaleTransferFunction = 'PiecewiseFunction'
tableToPoints1Display.OpacityArray = ['POINTS', 'Field 0']
tableToPoints1Display.OpacityTransferFunction = 'PiecewiseFunction'
tableToPoints1Display.DataAxesGrid = 'GridAxesRepresentation'
tableToPoints1Display.PolarAxes = 'PolarAxesRepresentation'

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
tableToPoints1Display.ScaleTransferFunction.Points = [-1.0, 0.0, 0.5, 0.0, 1.0, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
tableToPoints1Display.OpacityTransferFunction.Points = [-1.0, 0.0, 0.5, 0.0, 1.0, 1.0, 0.5, 0.0]

# reset view to fit data
renderView1.ResetCamera()

#changing interaction mode based on data extents
renderView1.InteractionMode = '2D'
renderView1.CameraPosition = [0.0, 0.0, 10000.0]

# get the material library
materialLibrary1 = GetMaterialLibrary()

# update the view to ensure updated data information
renderView1.Update()

# create a new 'Delaunay 2D'
delaunay2D1 = Delaunay2D(Input=tableToPoints1)

# show data in view
delaunay2D1Display = Show(delaunay2D1, renderView1)

# trace defaults for the display properties.
delaunay2D1Display.Representation = 'Surface'
delaunay2D1Display.ColorArrayName = [None, '']
delaunay2D1Display.OSPRayScaleArray = 'Field 0'
delaunay2D1Display.OSPRayScaleFunction = 'PiecewiseFunction'
delaunay2D1Display.SelectOrientationVectors = 'Field 0'
delaunay2D1Display.ScaleFactor = 0.2
delaunay2D1Display.SelectScaleArray = 'Field 0'
delaunay2D1Display.GlyphType = 'Arrow'
delaunay2D1Display.GlyphTableIndexArray = 'Field 0'
delaunay2D1Display.GaussianRadius = 0.01
delaunay2D1Display.SetScaleArray = ['POINTS', 'Field 0']
delaunay2D1Display.ScaleTransferFunction = 'PiecewiseFunction'
delaunay2D1Display.OpacityArray = ['POINTS', 'Field 0']
delaunay2D1Display.OpacityTransferFunction = 'PiecewiseFunction'
delaunay2D1Display.DataAxesGrid = 'GridAxesRepresentation'
delaunay2D1Display.PolarAxes = 'PolarAxesRepresentation'

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
delaunay2D1Display.ScaleTransferFunction.Points = [-1.0, 0.0, 0.5, 0.0, 1.0, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
delaunay2D1Display.OpacityTransferFunction.Points = [-1.0, 0.0, 0.5, 0.0, 1.0, 1.0, 0.5, 0.0]

# hide data in view
Hide(tableToPoints1, renderView1)

# update the view to ensure updated data information
renderView1.Update()

# set scalar coloring
ColorBy(delaunay2D1Display, ('POINTS', 'Field 13'))

# rescale color and/or opacity maps used to include current data range
delaunay2D1Display.RescaleTransferFunctionToDataRange(True, False)

# show color bar/color legend
delaunay2D1Display.SetScalarBarVisibility(renderView1, True)

# get color transfer function/color map for 'Field13'
field13LUT = GetColorTransferFunction('Field13')

# get opacity transfer function/opacity map for 'Field13'
field13PWF = GetOpacityTransferFunction('Field13')

#### saving camera placements for all active views

# current camera placement for renderView1
renderView1.InteractionMode = '2D'
renderView1.CameraPosition = [0.0, 0.0, 10000.0]
renderView1.CameraParallelScale = 1.4142135623730951