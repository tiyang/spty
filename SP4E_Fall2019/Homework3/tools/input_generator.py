import numpy as np
import argparse

# Argument Parser
my_parser = argparse.ArgumentParser()
my_parser.add_argument('-simulation', type=str, action='store', 
                       choices=['uniform_temperature', 'line_source', 'radial_source'],
                       default='line_source', 
                       help='The simulated test case.')
my_parser.add_argument('-N_grid', type=int, action='store', default= 32, help='The grid size in the x and y direction N_gridxN_grid.')
my_parser.add_argument('-T', type=float, action='store', default = 0.0, help='Uniform initial temperature.')
my_parser.add_argument('-L', type=float, action='store', default = 1.0, help='Extend of the domain in the x and y positive and negative direction from the origin [-L;L)x[-L;L).')
my_parser.add_argument('-R', type=float, action='store', default = 0.5, help='Radius size for the radial source test case.')

args    = my_parser.parse_args()
case    = args.simulation
N       = args.N_grid
L       = args.L
R       = args.R
Temp    = args.T

# Set positions over -L to L interval
x = np.linspace(-L, L, N)
y = np.linspace(-L, L, N)

X, Y = np.meshgrid(x, y)
Z = np.zeros((N, N))

# Set velocities, forces and mass (default at 0 as not used with material_point simulations)
V_x = np.zeros((N, N))
V_y = np.zeros((N, N))
V_z = np.zeros((N, N))

F_x = np.zeros((N, N))
F_y = np.zeros((N, N))
F_z = np.zeros((N, N))

M = np.zeros((N, N))

# Set temperature distribution and heat source for the different cases
if case == 'uniform_temperature':
    T = Temp*np.ones((N,N)) # uniform T
    H_v = np.zeros((N,N)) # no heat source
elif case == 'line_source':
    T = Temp*np.ones((N,N)) # uniform T
    quarter_L = int(round(N/4.0))
    three_qarter_L = int(round(3.0*N/4.0))
    dx = 2.0*L/N
    H_v = np.zeros((N, N))
    H_v[:, quarter_L] = -1.0/dx
    H_v[:, three_qarter_L] = 1.0/dx
elif case == 'radial_source':
    T = Temp*np.ones((N,N)) # uniform T
    H_v = np.zeros((N, N))
    dx = 2.0*L/N
    H_v[np.where((X**2 + Y**2) < R)] = 1/dx

# Rearrange the data to be written into the input csv file
X = X.flatten(order='C')
Y = Y.flatten(order='C')
Z = Z.flatten(order='C')
V_x = V_x.flatten(order='C')
V_y = V_y.flatten(order='C')
V_z = V_z.flatten(order='C')
F_x = F_x.flatten(order='C')
F_y = F_y.flatten(order='C')
F_z = F_z.flatten(order='C')
M = M.flatten(order='C')
T = T.flatten(order='C')
H_v = H_v.flatten(order='C')
output = np.column_stack((X, Y, Z, V_x, V_y, V_z, F_x, F_y, F_z, M, T, H_v))

column_header = 'X Y Z V_x V_y V_z F_x F_y F_z M T H_v'
np.savetxt('input.txt', output, delimiter=' ')

