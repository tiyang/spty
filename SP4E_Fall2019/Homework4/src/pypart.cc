#include <functional>
#include <iostream>
#include <memory>
#include <pybind11/pybind11.h>

#include "my_types.hh"
#include "fft.hh"
#include "system_evolution.hh"
#include "particles_factory_interface.hh"
#include "material_points_factory.hh"
#include "planets_factory.hh"
#include "ping_pong_balls_factory.hh"
#include "csv_writer.hh"
#include "compute.hh"
#include "compute_temperature.hh"
#include "compute_interaction.hh"
#include "compute_gravity.hh"
#include "compute_verlet_integration.hh"

namespace py = pybind11;

PYBIND11_MODULE(pypart, m) {
    m.doc() = "Python binding for the particles code.";

    // ========== Class System ==================================================================
    py::class_<System>(m, "System");



    // ========== Class SystemEvolution =========================================================
    py::class_<SystemEvolution, std::unique_ptr<SystemEvolution>>(m, "SystemEvolution",
                                                                  py::dynamic_attr())
        .def(py::init([](std::unique_ptr<System>()){return std::unique_ptr<SystemEvolution>();}),
             py::return_value_policy::move)
        .def("evolve",      &SystemEvolution::evolve)
        .def("addCompute",  &SystemEvolution::addCompute)
        .def("getSystem",   &SystemEvolution::getSystem, py::return_value_policy::reference)
        .def("setNSteps",   &SystemEvolution::setNSteps)
        .def("setDumpFreq", &SystemEvolution::setDumpFreq);



    // ========== Class ParticlesFactoryInterface ===============================================
    py::class_<ParticlesFactoryInterface>(m, "ParticlesFactoryInterface",
                                          py::dynamic_attr())
        .def("getInstance", &ParticlesFactoryInterface::getInstance,
                    py::return_value_policy::reference)
        .def_property_readonly("system_evolution",
                    &ParticlesFactoryInterface::getSystemEvolution);



    // ========== Class MaterialPointsFactory ===================================================
    py::class_<MaterialPointsFactory,ParticlesFactoryInterface>(m, "MaterialPointsFactory",
                                                                py::dynamic_attr())
        .def("createSimulation", py::overload_cast<const std::string&, Real, py::function>
                    (&ParticlesFactoryInterface::createSimulation<py::function>), py::return_value_policy::reference)
        .def("getInstance", &MaterialPointsFactory::getInstance,
                    py::return_value_policy::reference);



    // ========== Class PlanetsFactory ==========================================================
    py::class_<PlanetsFactory, ParticlesFactoryInterface>(m, "PlanetsFactory",
                                                          py::dynamic_attr())
        .def("createSimulation", py::overload_cast<const std::string&, Real, py::function>
                    (&ParticlesFactoryInterface::createSimulation<py::function>), py::return_value_policy::reference)
        .def("getInstance", &PlanetsFactory::getInstance,
                    py::return_value_policy::reference);


    // ========== Class PingPongBallsFactory ====================================================
    py::class_<PingPongBallsFactory, ParticlesFactoryInterface>(m, "PingPongBallsFactory",
                                                                py::dynamic_attr())
    .def("createSimulation", py::overload_cast<const std::string&, Real, py::function>
                (&ParticlesFactoryInterface::createSimulation<py::function>), py::return_value_policy::reference)
    .def("getInstance", &PingPongBallsFactory::getInstance,
                py::return_value_policy::reference);





    // ========== Class Compute =================================================================
    py::class_<Compute, std::shared_ptr<Compute>>(m, "Compute");

    class Pycompute : public Compute{
    public:
        using Compute::Compute;
        void compute(System &system) override{
            PYBIND11_OVERLOAD_PURE(void, Compute, compute, system);
        }
    };



    // ========== Class ComputeTemperature ======================================================
    py::class_<ComputeTemperature, Compute,
                    std::shared_ptr<ComputeTemperature>>(m, "ComputeTemperature",
                                                         py::dynamic_attr())
        .def(py::init<>())
        .def("compute", &ComputeTemperature::compute,
             py::return_value_policy::reference)
        .def_property_readonly("conductivity", &ComputeTemperature::getConductivity)
        .def_property_readonly("capacity",     &ComputeTemperature::getCapacity)
        .def_property_readonly("density",      &ComputeTemperature::getDensity)
        .def_property_readonly("L",            &ComputeTemperature::getL)
        .def_property_readonly("deltat",       &ComputeTemperature::getDeltat);



    // ========== Class ComputeInteraction ======================================================
    py::class_<ComputeInteraction, Compute,
                    std::shared_ptr<ComputeInteraction>>(m, "ComputeInteraction",
                                                         py::dynamic_attr());



    // ========== Class ComputeGravity ==========================================================
    py::class_<ComputeGravity, ComputeInteraction,
                std::shared_ptr<ComputeGravity>>(m, "ComputeGravity",
                                                     py::dynamic_attr())
    .def(py::init<>())
    .def("compute", &ComputeGravity::compute,
         py::return_value_policy::reference)
    .def("setG",   &ComputeGravity::setG);


    // ========== Class ComputeVerletIntegration ================================================
    py::class_<ComputeVerletIntegration, Compute,
                std::shared_ptr<ComputeVerletIntegration>>(m, "ComputeVerletIntegration",
                                                           py::dynamic_attr())
    .def(py::init<Real>())
    .def("compute", &ComputeVerletIntegration::compute,
         py::return_value_policy::reference)
    .def("addInteraction", &ComputeVerletIntegration::addInteraction)
    .def("setDeltaT",      &ComputeVerletIntegration::setDeltaT);


    // ========== Class CsvWriter ===============================================================
    py::class_<CsvWriter,Compute,std::shared_ptr<CsvWriter>>(m,"CsvWriter")
        .def(py::init<const std::string &>())
        .def("write", &CsvWriter::write);
}
