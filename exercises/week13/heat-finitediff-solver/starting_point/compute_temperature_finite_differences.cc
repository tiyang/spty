#include "compute_temperature_finite_differences.hh"
#include "material_point.hh"
#include "matrix.hh"

void ComputeTemperatureFiniteDifferences::compute(System& system) {}

void ComputeTemperatureFiniteDifferences::assembleLinearOperator(
    System& system) {}

void ComputeTemperatureFiniteDifferences::assembleRightHandSide(
    System& system) {}
