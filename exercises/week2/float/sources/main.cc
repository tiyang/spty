/* -------------------------------------------------------------------------- */
#include <cstdlib>
#include <cmath>
#include <iostream>
/* -------------------------------------------------------------------------- */

void exo1(){
    float a =1/2;
    std::cout << a <<std::endl;
}
/* -------------------------------------------------------------------------- */
double tan1(double x){
        double Result = (exp(x) - exp(-x))/(exp(x) + exp(-x));
        return Result;
    }

double tan2(double x){
    double Result = (1 - exp(-2*x))/(1+exp(-2*x));
    return Result;
}
void exo2(){
    for(int i = 0; i<=10; i++){
        double x = i*100.;
        double tan2x = tan2(x);
        std::cout << "x: " << x << " tan(x): " << tan2x << std::endl;
    }
}

double tan3(double x){
    double Result = (-2*exp(-2*x))/(1+exp(-2*x));
    return Result;
}
/* -------------------------------------------------------------------------- */
double evolution(double pn, double v, double dt){
    double pn1 = pn + dt*v;
    return pn1;
}
void exo3() {
    for (int i = 0; i <= 10; i++) {
        double x = i * 100.;
        double tan3x = tan3(x);
        std::cout << "x: " << x << " tan(x): " << tan3x << std::endl;
    }
    double p0 = 0, p1 = 0.001, dt = 1000., v = 3.0e8, distance;
    for (int i = 1; i<=40; i++){
        distance = p1 - p0;
        p0 = evolution(p0,v,dt);
        p1 = evolution(p1,v,dt);
        std::cout<< "Distance with i ="<<i<<" : "<<distance<<std::endl;
    }
}
/* -------------------------------------------------------------------------- */


int main(int argc, char ** argv){


  exo1();
  exo2();
  exo3();

  return EXIT_SUCCESS;
}



