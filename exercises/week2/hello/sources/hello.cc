#include <string>
#include <iostream>
#include "series.hh"
/* -------------------------------------------------------------------------- */


int main(int argc, char ** argv){
  int N,S;
  N = std::stoi(argv[1]);
  S = computeSeries(N);
  std::cout<<"Hello "<<S<<std::endl;
  return EXIT_SUCCESS;
}
