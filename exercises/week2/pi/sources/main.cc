/* -------------------------------------------------------------------------- */
#include <cstdlib>
#include <cmath>
#include <iostream>
#include <iomanip>
/* -------------------------------------------------------------------------- */
float CalPi(int k){
    double pi;
    for(int i = 1; i <=k; i++) {
        pi = pi + 6. / pow(i, 2);
    }
    pi = sqrt(pi);
    return pi;
}

int main(int argc, char ** argv){
    float pi;
    int ite = std::stoi(argv[1]);
    pi = CalPi(ite);
    std::cout<<"iteration: "<<ite<< " pi: "<<pi<<std::endl;
  return EXIT_SUCCESS;
}



