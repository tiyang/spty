#include <cstdlib>
#include <string>
#include <vector>
#include <algorithm>
#include <cmath>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <map>
#include <array>
#include <set>
#include <iterator>
#include <numeric>

int main (){
    int n = 1000;
    std::vector<double> vec(n);
    // step1
    std::fill(vec.begin(),vec.end(),0);
    // step2
    std::iota(vec.begin(),vec.end(),1);
    for( auto & i : vec){
        std::cout << i << " ";
    }
    std::cout << std::endl;
    //step3
    auto sq = [](double &arg){arg = arg*arg; return arg;};
    double arg1 = 1.2;
    auto test_sq = sq(arg1);
    std::cout << "test_sq: " << test_sq <<std::endl;
    //step4
    for( auto & i : vec){
        auto j = sq(i);
        std::cout << i << " " << j << ", ";
    }
    std::cout << std::endl;
    //step5
    //auto sq2 = [](double &arg){double arg2 = arg*arg; std::cout << arg2 <<" "; return arg2;};
    //std::for_each(vec.begin(),vec.end(),sq2);
    //step6
    double sum1 = 0.;
    auto sumvec = [&sum1](double & i){ sum1 = sum1+i; };
    std::for_each(vec.begin(),vec.end(),sumvec);
    std::cout << "sum: " << sum1 << std::endl;
    std::cout << "exact sum: " << n*(n+1)*(2*n+1)/6.<<std::endl;
    // step7
    double sum2 ;
    sum2 = std::accumulate(vec.begin(),vec.end(),0.);
    std::cout << sum2<<std::endl;
    // step8
    auto invert = [](double &i){ i = 1./i;};
    std::for_each(vec.begin(),vec.end(),invert);
    double pi = 0.;
    pi = std::accumulate(vec.rbegin(),vec.rend(),0.);
    pi = std::sqrt(6. * pi);
    std::cout << pi << " vs " << M_PI <<std::endl;
return 0;
}