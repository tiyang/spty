#include <cstdlib>
#include <string>
#include <vector>
#include <algorithm>
#include <cmath>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <map>
#include <array>
#include <set>
#include <iterator>

/* -------------------------------------------------------------------------- */
using Point = std::array<double,3>;
int main(int argc, char ** argv){
    // Exercise 1
    if (argc != 2){
        std::cout << "Please input your name correctly";
    }

    std::string myName = "Tian";
    std::string argument = std::string(argv[1]);
    if (argument == myName){
        std::cout << "You have a nice name" << std::endl;
    }

    // Exercise 2
   /* std::string Coord = "1.5 1.0 2.0";
    std::ofstream toto("toto.txt");
    toto<< Coord << std::endl;
    */

    std::ifstream toto("toto.txt");
    char str[40];
    std::stringstream Coord;
    toto.getline(str,40);
    Coord << str;
    double x,y,z;
    Coord >>x >> y >> z;
    std::cout<<"x: "<<x<<"  y: "<<y<<"  z: "<<z<<std::endl;

    // Exercise 3
    std::array<double,3> toto_array = {x,y,z};
    std::cout << "array[0] = " << toto_array[0]
    <<" array[1] = " << toto_array[1]
    << " array[2] = " << toto_array[2] << std::endl;

    std::vector<double> pi_vector;
    double inc = 2.0*M_PI/100.0;
    for(int i = 1; i <= 100; i++){
        pi_vector.push_back(i*inc);
    }
    std::cout <<"The size of vector: "<< pi_vector.size() <<std::endl;
    std::cout <<"pi_vector[0]: "<<pi_vector[0]<<std::endl;
    //pi_vector.resize(120);
    //std::cout<<"Extra Part of the vector: "<<pi_vector[110];
    std::vector<std::pair<double,double>> sin_vect;
    for(int i = 1; i <= 100; i++){
         sin_vect.push_back(std::make_pair(pi_vector[i-1],sin(pi_vector[i-1])));
    }
    std::cout <<"sin_vector[0]: "<<sin_vect[0].first <<std::endl;
    std::ofstream sin_pi("sin_pi.txt");
    for(int i = 0; i < 100; i++){
        sin_pi << std::scientific<<std::setprecision(20)
        <<sin_vect[i].first<<"  "<<sin_vect[i].second <<std::endl;
    }
    sin_pi.close();

    //exercise 5
    std::map<std::string, Point> map1;
    Point sun = {0.,0.,0.};
    Point earth = {1.,0.,0.};
    Point mercury = {0.25,0.,0.};
    Point jupiter = {5.,0.,0.};
    map1["sun"] = sun;
    map1["earth"] = earth;
    //auto it = map1.find("earth");
    //map1["earth"] = mercury;
    for(auto & planet: map1){
        auto & name = planet.first;
        auto & values = planet.second;
        std::cout << name << std::endl;
        std::cout << values[0] <<" " << values[1]<< " " <<values[2]<< std::endl;
    }
    std::map<std::string, Point> map2 = {{"mercury",mercury},{"earth",earth},{"jupiter",jupiter}};
    map2["sun"] = sun;
    std::set<std::string> key1, key2, key_intersect;
    for(auto & planet:map1){
        auto &name = planet.first;
        key1.insert(name);
    }
    for(auto & planet:map2){
        auto &name = planet.first;
        key2.insert(name);
    }

    std::set_intersection(key1.begin(),key1.end(),key2.begin(),key2.end(),
                          std::inserter(key_intersect,key_intersect.begin()));
    std::map<std::string, Point> map_insert;
    for(auto & name : key_intersect){
        map_insert[name] = map2[name];
    }
    for(auto & planet: map_insert){
        auto & name = planet.first;
        auto & values = planet.second;
        std::cout << name << std::endl;
        std::cout << values[0] <<" " << values[1]<< " " <<values[2]<< std::endl;
    }
    return EXIT_SUCCESS;


}



