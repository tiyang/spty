#include <vector>
#include <array>
#include <memory>
#include <iostream>

int main() {
    // static
    /*
  int values[10];
  for (int& v : values)
    v = 0;
     */

    // heap
    int *values = new int [10];

  for (int i = 0; i < 10; ++i)
    std::cout << values[i] << " ";
  std::cout << std::endl;
  delete[] values;
  return 0;
}
