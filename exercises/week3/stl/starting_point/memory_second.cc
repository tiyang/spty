#include <vector>
#include <array>
#include <memory>
#include <iostream>

std::vector<int>* stack_allocation(int n) {
    std::vector<int> stack_vector(n);
    return &stack_vector;
}

std::vector<int>* heap_allocation(int n) {
    std::vector<int> *heap_vector = new std::vector<int>(n);
    return heap_vector;
}

int main() {
  std::vector<int> * stack_values = stack_allocation(10);
  std::vector<int> * heap_values = heap_allocation(10);

  //std::cout << stack_values->size() << ", " << heap_values->size() << std::endl;
  std::cout << heap_values->size() << std::endl;
  delete heap_values;
  return 0;
}
