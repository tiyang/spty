#!/usr/bin/env python3

import numpy as np


def solve_GMRES(A, b, x0, max_iter=30, tol=1e-15, callback=None):

    # Initialisation
    r = b - np.einsum('ij, j->i', A, x0)
    h = np.zeros((max_iter+1, max_iter))
    q = np.array([0 for i in range(max_iter)], dtype=object)
    q[0] = r/np.linalg.norm(r)
    n = min(max_iter, len(b))

    for k in range(n):

        # Arnoldi iteration
        Q, h = arnoldi_iteration(A, q, h, k, max_iter)

        # least square solution
        x, sol, dependent_variable = least_square_solution(
            r, max_iter, h, q, x0, callback=callback)

        # Calcul of the residual
        residual = np.linalg.norm(np.einsum('ij, j->i', h, sol)-np.linalg.norm(dependent_variable))/np.linalg.norm(dependent_variable)
        if residual < tol:
            return x, 0

    return x, 1


def arnoldi_iteration(A, q, h, k, max_iter):

    r_new = np.einsum('ij, j -> i', A, q[k])
    for _l in range(k+1):
        h[_l, k] = np.einsum('i, i', q[_l], r_new)
        r_new = r_new - q[_l]*h[_l, k]
    h[k+1, k] = np.linalg.norm(r_new)
    if (h[k + 1, k] != 0 and k != max_iter - 1):
        q[k + 1] = r_new/h[k + 1, k]

    return q, h


def least_square_solution(r, max_iter, h, q, x0, callback=None):

    dependent_variable = np.zeros(max_iter + 1)
    dependent_variable[0] = np.linalg.norm(r)
    sol = np.linalg.lstsq(h, dependent_variable, rcond=None)[0]
    x = np.dot(np.transpose(q), sol) + x0
    if callback is not None:
        callback(x)
    return x, sol, dependent_variable
