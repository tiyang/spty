import series
import numpy as np
import matplotlib.pyplot as plt
class DumperSeries:

    def __init__(self, series):
        self.series = series

    def dump(self):
        raise Exception("pure virtual function")

class PrintSeries (DumperSeries):

    def __init__(self, series, maxiter, frequency = 1):
        super().__init__(series)
        self.frequency = frequency
        self.maxiter = maxiter

    def dump(self):
        i = 1
        while (i < self.maxiter):
            print("N = {0}, Sn = {1}" .format(i, self.series.compute(i)) )
            i += self.frequency
        print("N = {0}, Sn = {1}" .format(self.maxiter, self.series.compute(self.maxiter)))
        if isinstance(self.series, series.ComputePi):
            print("Analytical Prediction = {}" .format(self.series.getAnalyticPrediction()))

class PlotSeries(DumperSeries):

    def __init__(self, series, maxiter, frequency = 1):
        super().__init__(series)
        self.frequency = frequency
        self.maxiter = maxiter

    def dump(self):
        x = np.array([])
        y = np.array([])
        i = 1
        while (i < self.maxiter):
            x = np.append(x,i)
            y = np.append(y, self.series.compute(i))
            i += self.frequency
        x = np.append(x, self.maxiter)
        y = np.append(y, self.series.compute(self.maxiter))
        fig, ax = plt.subplots()
        ax.plot(x,y, 'b')
        ax.set_xlabel('n')
        ax.set_ylabel('Sn')
        if isinstance(self.series, series.ComputePi):
            x = np.array([0,self.maxiter+10])
            y = np.repeat(self.series.getAnalyticPrediction(),2)
            ax.plot(x,y,'--r')
        ax.set_xlim((0,self.maxiter))
        plt.show()

