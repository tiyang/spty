from series import ComputeArithmetic
from series import ComputePi
from dumper_series import PrintSeries
from dumper_series import PlotSeries
if __name__ == '__main__':
    FuncName = input("Enter the function name: ")
    if FuncName == 'CA':
        C = ComputeArithmetic()
    elif FuncName == 'CP':
        C = ComputePi()
    else:
        print("No such function")
        exit
    maxiter = 100
    frequency = 1
    PrS = PrintSeries(C, maxiter, frequency)
    PlS = PlotSeries(C, maxiter, frequency)
    #PrS.dump()
    PlS.dump()
