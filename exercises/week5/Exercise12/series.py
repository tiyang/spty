import math

class Series:

    def __init__(self):
        self.current_term = 0
        self.current_value = 0;

    def compute(self, N):
        raise Exception("pure virtual function")

    def getAnalyticPrediction(self):
        raise Exception("pure virtual function")

class ComputeArithmetic (Series):

    def __init__(self):
        Series.__init__(self)

    def compute(self, N):
        Sn = 0
        for i in range(1, N+1):
            Sn += i
        return Sn

class ComputePi (Series):

    def __init__(self):
        super().__init__()

    def compute(self, N):
        pi = 0
        for i in range(1, N+1):
            pi = pi + 1/(i**2)
        pi = (pi*6)**0.5
        return pi

    def getAnalyticPrediction(self):
        return math.pi

