from series import ComputeArithmetic
from series import ComputePi
from dumper_series import PrintSeries
from dumper_series import PlotSeries
if __name__ == '__main__':
    FuncName = input("Enter the function name: ")
    if FuncName == 'CA':
        C1 = ComputeArithmetic()
        C2 = ComputeArithmetic()
    elif FuncName == 'CP':
        C1 = ComputePi()
        C2 = ComputePi()
    else:
        print("No such function")
        exit
    maxiter = 100
    frequency = 2
    PrS = PrintSeries(C1, maxiter, frequency)
    PlS = PlotSeries(C2, maxiter, frequency)
    PrS.dump()
    PlS.dump()
