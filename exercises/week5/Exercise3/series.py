import math

class Series:

    def __init__(self):
        self.current_term = 0
        self.current_value = 0;

    def compute(self, N):
        raise Exception("pure virtual function")

    def getAnalyticPrediction(self):
        raise Exception("pure virtual function")

class ComputeArithmetic (Series):

    def __init__(self):
        Series.__init__(self)

    def compute(self, N):
        Sn = self.current_value
        for i in range(self.current_term + 1, N+1):
            self.current_term = i
            Sn += i
        self.current_value = Sn
        return Sn

class ComputePi (Series):

    def __init__(self):
        super().__init__()

    def compute(self, N):
        pi = self.current_value
        pi = pi**2
        for i in range(self.current_term + 1, N+1):
            self.current_term = i
            pi = pi + 6/(i**2)
        pi = pi**0.5
        self.current_value = pi
        return pi

    def getAnalyticPrediction(self):
        return math.pi

