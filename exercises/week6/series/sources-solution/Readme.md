# Instructions to compile the code

Compilation:

mkdir build
cd build
ccmake ../
make

# Instructions to run the program:

In the build/src directory:

In order to get the help:

./compute_series --help

The arguments that can be provided are the following :

- --series_type : pi/arithmetic/integral (Default pi)

- --function : cube/cos/sin

- --dumper_type : print or write (Default print)

- --output : cout or filename (Default cout)

- --freq : int, output frequency of the serie value. (Default 10)

- --maxiter: int, maximal series iteration. (Default 1000)

- --precision: number of digits to use (Default 5)

- --delimiter: separator to use for produced files (Default " ")

- --integral_bound_inf: bound inf of integral

- --integral_bound_sup: bound sup of integral

In order to run the program, at least one argument has to be provided.

Examples:

- ./compute_series --series_type arithmetic --dumper_type write

- ./compute_series --series_type integral --function sin --dumper_type write --integral_bound_inf 0 --integral_bound_sup pi/2

