#ifndef __PARTICLES_FACTORY_INTERFACE__HH__
#define __PARTICLES_FACTORY_INTERFACE__HH__

/* -------------------------------------------------------------------------- */
#include "system_evolution.hh"
/* -------------------------------------------------------------------------- */

//! Abstract factory defining interface
class ParticlesFactoryInterface {
  // Constructors/Destructors
protected:
  //! Instance constructor (protected)
  ParticlesFactoryInterface() = default;

public:
  virtual ~ParticlesFactoryInterface() = default;

  // Methods
public:
  //! Create a whole simulation from file
  virtual SystemEvolution& createSimulation(const std::string& fname,
                                            Real timestep) = 0;
  //! Create a new particle
  virtual std::unique_ptr<Particle> createParticle() = 0;
  /* When using unique_ptr, there can be at most one unique_ptr pointing at any one resource.
   *  unique_ptr can be moved with std::move
   *  shared_ptr allows for multiple pointers to point at a given resource.
   */

  //! Get singleton instance
  static ParticlesFactoryInterface& getInstance(); // return the static class object
  /*
   * A static member function can be called even if no objects of the class exist
   * static functions are accessed using only the class name and the scope resolution operator ::
   * A static member function can only access static data member, other static member functions and any other functions from outside the class.
   */

  // Members
protected:
  std::vector<Particle*> list_particles;
  std::unique_ptr<SystemEvolution> system_evolution = nullptr;

  // Standard pointer because constructor is protected (cannot use make_unique)
  static ParticlesFactoryInterface* factory;
};

/* -------------------------------------------------------------------------- */
#endif  //__PARTICLES_FACTORY_INTERFACE__HH__
