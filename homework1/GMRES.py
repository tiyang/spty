import numpy as np
import argparse
import optimizer

def Sx(x,A,b):   # the target function for minimizing
    return 0.5*np.dot(x,A)@x - x@b

def GMRES(A, b, x0, maxiter, tol) :
    r = b - np.einsum('ij,j->i',A,x0)
    x = np.array([x0]).reshape(1,-1)
    H = np.zeros((2,1))
    Q = (r/np.linalg.norm(r)).reshape(-1,1)
    for k in range(maxiter):
        Q, H = Arnold_It(A, Q, H, k)
        beta = np.zeros(H.shape[0])
        beta[0] = np.linalg.norm(r)
        yn = np.linalg.lstsq(H,beta,rcond=None)[0]
        xn = np.einsum('ij,j->i',Q[:,:-1],yn) + x0
        x = np.append(x,xn.reshape(1,-1),axis=0)
        rn = np.einsum('ij,j->i',A,xn) - b
        if np.linalg.norm(rn)/np.linalg.norm(xn) <= tol:
            print("Converged successfully")
            print("Total iteration: {}" .format(k+1))
            return x
        elif k == maxiter-1:
            print("Not convergent for the defined maxiter")

def Arnold_It (A, Q, H, k):
    q = np.einsum('ij,j->i',A,Q[:,k])
    if k!= 0:
        H = np.append(H,np.zeros((H.shape[0],1)),axis = 1)
        H = np.append(H,np.zeros((1,H.shape[1])),axis = 0)
    for i in range(k+1):
        H[i,k] = np.einsum('i,i',q,Q[:,i])
        q = q - H[i,k]*Q[:,i]
    H[k+1,k] = np.linalg.norm(q)
    q = q/H[k+1,k]
    Q = np.append(Q,q.reshape(-1,1),axis=1)
    return Q, H

if __name__ == '__main__' :
    parser = argparse.ArgumentParser()
    parser.add_argument("-A", type=str, required = True,
                    help="<Required> the matrix A, Input format: '1, 2; 3 4'")
    parser.add_argument("-b", type=float, required = True, nargs="+",
                    help="<Required> the matrix b, Input format: 1 2")
    parser.add_argument("-p", "--plot", action="store_true",
                    help="Plot the result or not (only for 3D systems)")
    parser.add_argument("--solver", default='GMRES',const='GMRES', nargs = '?',
                    choices=['GMRES', 'BFGS', 'LGMRES'], help = 'Default = GMRES, Choose the solver you want')
    args = parser.parse_args()
    A = np.array(np.matrix(args.A))
    b = np.array(args.b)
    x0 = np.zeros(b.shape[0])
    if args.solver == 'GMRES':
        x = GMRES(A,b,x0,5,1e-5)
    elif args.solver == 'BFGS':
        x = optimizer.mini_BFGS(Sx,A,b,x0)
    elif args.solver == 'LGMRES':
        x = optimizer.solve_GMRES(A,b,x0)
    if args.plot:
        optimizer.plot3D(A,b,x,args.solver)



