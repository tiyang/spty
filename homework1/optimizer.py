import numpy as np
import scipy.optimize
import scipy.sparse.linalg
import matplotlib.pyplot as plt
from matplotlib import cm
import argparse

def Sx(x,A,b):   # the target function for minimizing
    return 0.5*np.dot(x,A)@x - x@b

def callbackfun(x):  # this function is called for recording in each iteration
    global x_iter
    x_iter = np.append(x_iter, x.reshape(1,-1),axis=0)

def mini_BFGS(func,A,b,xs):
    global x_iter
    x_iter = xs.reshape(1, -1)  # x_iter is used for recording the value of x in each iteration
    res = scipy.optimize.minimize(fun = func, x0 = xs,args=(A,b), callback = callbackfun,
                              method = 'BFGS',tol=1e-7, options={'maxiter':1000})
    print('BFGS: {}'.format(res.message))
    return x_iter

def solve_GMRES(A,b,xs):
    global x_iter
    x_iter = xs.reshape(1, -1)  # x_iter is used for recording the value of x in each iteration
    res, info = scipy.sparse.linalg.lgmres(A = A,b = b,x0 = xs,tol = 1e-7,atol=1e-7,maxiter=500,callback=callbackfun)
    if info == 0:
        print("GMRES: Succeeded")
    elif info > 0:
        print("GMRES: Convergence to tolerance not achievd")
    else:
        print("GMRES: Illegal input or breakdown")
    return x_iter

def plot3D(A,b,x,name):
    if b.shape[0] != 2:
        print('The dimension of system is not 3D')
        return
    else:
        fig = plt.figure()
        ax1 = fig.add_subplot(projection = '3d')
        nf = 200
        X = np.linspace(-3.1,3.1,nf)
        Y = np.linspace(-1.8,4.4,nf)
        X,Y = np.meshgrid(X, Y)
        Z = np.zeros((nf,nf))
        for i in range(0,nf):
            for j in range(0,nf):
                Z[i,j] = Sx(np.array([X[i,j],Y[i,j]]),A,b)
        # plot the surface of S(x)
        ax1.plot_surface(X, Y, Z, rstride=10, cstride=10,
                               linewidth = 0, alpha = 0.5, cmap = cm.coolwarm)
        # plot the contour of S(x) in 3D
        ax1.contour3D(X, Y, Z,stride = 1, colors = 'b')
        # plot the iteration path
        z1 = np.zeros(x.shape[0])
        for i in range(0,x.shape[0]):
            z1[i] = Sx(x[i,:],A,b)
        ax1.plot(x[:,0],x[:,1],z1, marker='.',
                 c = 'r',ls = '--', label = 'Iteration Path')
        # plot setting
        ax1.legend(loc = "upper left")
        ax1.view_init(elev=65,azim=60)
        ax1.set_xlim(-3,3)
        ax1.set_ylim(-1.7,4.3)
        ax1.set_zlim(0,70)
        ax1.set_xlabel('x1')
        ax1.set_ylabel('x2')
        ax1.set_zlabel('S(x)')
        ax1.set_title(name)
        ax1.set_zticks(np.arange(0,70,20))
        plt.show()

if __name__ == '__main__' :
    parser = argparse.ArgumentParser()
    parser.add_argument("-A", type=str, required = True,
                    help="<Required> the matrix A, Input format: '1, 2; 3 4'")
    parser.add_argument("-b", type=float, required = True, nargs="+",
                    help="<Required> the matrix b, Input format: 1 2")
    parser.add_argument("-p", "--plot", action="store_true",
                    help="Plot the result or not (only for 3D systems)")
    parser.add_argument("--solver", default='BFGS',const='BFGS', nargs = '?',
                    choices=['BFGS', 'LGMRES'], help = 'Default = BFGS, Choose the solver you want')
    args = parser.parse_args()
    A = np.array(np.matrix(args.A))
    b = np.array(args.b)
    x0 = np.zeros(b.shape[0])
    if args.solver == 'BFGS':
        x = mini_BFGS(Sx,A,b,x0)
    elif args.solver == 'LGMRES':
        x = solve_GMRES(A,b,x0)
    if args.plot:
        plot3D(A,b,x,args.solver)

    # #plot figure
    # fig = plt.figure()
    # ax1 = fig.add_subplot(121,projection = '3d')
    # ax2 = fig.add_subplot(122,projection = '3d')
    # nf = 200
    # X = np.linspace(-3.1,3.1,nf)
    # Y = np.linspace(-1.8,4.4,nf)
    # X,Y = np.meshgrid(X, Y)
    # Z = np.zeros((nf,nf))
    # for i in range(0,nf):
    #     for j in range(0,nf):
    #         Z[i,j] = Sx(np.array([X[i,j],Y[i,j]]),A,b)
    # surf1 = ax1.plot_surface(X, Y, Z, rstride=10, cstride=10,
    #                        linewidth = 0, alpha = 0.5, cmap = cm.coolwarm)
    # surf2 = ax2.plot_surface(X, Y, Z, rstride=10, cstride=10,
    #                         linewidth = 0, alpha = 0.5, cmap = cm.coolwarm)
    #
    # cont1 = ax1.contour3D(X, Y, Z,stride = 1, colors = 'b')
    # cont2 = ax2.contour3D(X, Y, Z,stride = 1, colors = 'b')
    #
    # z1 = np.zeros(x_iter_BFGS.shape[0])
    # z2 = np.zeros(x_iter_GMRES.shape[0])
    # for i in range(0,x_iter_BFGS.shape[0]):
    #     z1[i] = Sx(x_iter_BFGS[i,:],A,b)
    # for i in range(0,x_iter_GMRES.shape[0]):
    #     z2[i] = Sx(x_iter_GMRES[i,:],A,b)
    # ax1.plot(x_iter_BFGS[:,0],x_iter_BFGS[:,1],z1, marker='.',
    #          c = 'r',ls = '--', label = 'Iteration Path')
    # ax2.plot(x_iter_GMRES[:,0],x_iter_GMRES[:,1],z2, marker='.',
    #          c = 'r',ls = '--', label = 'Iteration Path')
    # ax1.legend(loc = "upper left")
    # ax2.legend(loc = "upper left")
    # ax1.view_init(elev=65,azim=60)
    # ax2.view_init(elev=65,azim=60)
    # ax1.set_xlim(-3,3)
    # ax1.set_ylim(-1.7,4.3)
    # ax1.set_zlim(0,70)
    # ax1.set_xlabel('x1')
    # ax1.set_ylabel('x2')
    # ax1.set_zlabel('S(x)')
    # ax1.set_title('BFGS')
    # ax2.set_xlim(-3,3)
    # ax2.set_ylim(-1.7,4.3)
    # ax2.set_zlim(0,70)
    # ax2.set_xlabel('x1')
    # ax2.set_ylabel('x2')
    # ax2.set_zlabel('S(x)')
    # ax2.set_title('GMRES')
    # ax1.set_zticks(np.arange(0,70,20))
    # ax2.set_zticks(np.arange(0,70,20))
    # plt.show()