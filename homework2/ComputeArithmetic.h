#ifndef COMPUTEARITHMETIC_H
#define COMPUTEARITHMETIC_H

#include "Series.h"

class ComputeArithmetic : public Series {
public:

    double getAnalyticPrediction() override;
    double computeTerm(unsigned int k) override;
};


#endif
