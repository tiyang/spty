
#ifndef COMPUTEPI_H
#define COMPUTEPI_H

#include "Series.h"

class ComputePi : public Series{
public:
    double compute(unsigned int N) override;
    double getAnalyticPrediction() override;
    double computeTerm(unsigned int k) override;
};


#endif
