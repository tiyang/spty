
#ifndef DUMPERSERIES_H
#define DUMPERSERIES_H

#include "Series.h"
#include <iostream>
class DumperSeries {

public:
    DumperSeries(Series &s): series(s){};
    virtual void dump(std::ostream & os) = 0;
    virtual void  setPrecision(unsigned int precision) {
        this -> precision = precision;
    };
protected:
    Series & series;
    unsigned int precision = 3;

};

inline std::ostream  &operator<< (std::ostream & stream, DumperSeries & _this) {
        _this.dump(stream);
        return stream;
    }

#endif
