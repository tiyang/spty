
#include "PrintSeries.h"
#include <iostream>
#include <iomanip>
#include <cmath>
PrintSeries :: PrintSeries(unsigned int f, unsigned int m, Series &s):DumperSeries(s) {
    this -> frequency = f;
    this -> maxiter = m;
}

void PrintSeries :: dump(std::ostream & os)  {
    double res1 = 0;
    double res0 = 0;
    double converg = 0;
    int i = 0;
    while ( i* this->frequency + 1 < this -> maxiter){
        res1 = this -> series.compute(i* this->frequency + 1);
        converg = res1 - res0;
        double ana = this -> series.getAnalyticPrediction();
        if (std::isnan(ana)){
            os << "k = " << i* this->frequency + 1 <<" result = " <<res1 << std::endl;
        } else{
            double err = ana - res1;
            os << std::scientific << std::setprecision(this ->precision);
            os << "k = " << i* this->frequency + 1 <<" result = " <<res1 << " ";
            os << "analytic: " << ana << " ";
            os << "error: " << err << " ";
            os << "converge: " << converg << std::endl;
        }
        i += 1;
        res0 = res1;
    }
}