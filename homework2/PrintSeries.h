
#ifndef PRINTSERIES_H
#define PRINTSERIES_H

#include "DumperSeries.h"

class PrintSeries : public DumperSeries{
public:
    PrintSeries(unsigned int f, unsigned int m, Series &s);
    void dump(std::ostream & os = std::cout ) override;

public:
    unsigned int frequency;
    unsigned int maxiter;
};


#endif
