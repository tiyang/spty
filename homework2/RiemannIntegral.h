
#ifndef RIEMANNINTEGRAL_H
#define RIEMANNINTEGRAL_H

#include "Series.h"
#include <string>
class RiemannIntegral: public Series {
public:
    RiemannIntegral(double a, double b, std::string f);
    double func (double x) ;
    double compute (unsigned int N) override;
    double getAnalyticPrediction () override;
public:
    double a;
    double b;
    std::string f;
};


#endif
