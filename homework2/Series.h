
#ifndef SERIES_H
#define SERIES_H


class Series {
public:
    virtual double compute (unsigned  int N)  ;
    virtual double getAnalyticPrediction() ;
    void addTerm() ;
    virtual double computeTerm(unsigned int k) {return 0;};
public:
    unsigned int current_index;
    double current_value;
};


#endif
