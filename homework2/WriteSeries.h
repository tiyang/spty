
#ifndef WRITESERIES_H
#define WRITESERIES_H

#include "DumperSeries.h"
#include <iostream>
class WriteSeries : public DumperSeries{
public:
    WriteSeries(unsigned int f, unsigned int m, Series &s, std::string sep = " ");
    void dump(std::ostream & os = std::cout) override;
    std::string setSeparator ();

public:
    unsigned int frequency;
    unsigned int maxiter;
    std::string seperator;
};


#endif
