/** -------------------------------------------------------------------------- 
HW2 - Classes in C++ - Oct 2021

Students:
Zheng Zheng    zheng.zheng@epfl.ch
Tian Yang      tian.yang@epfl.ch
-------------------------------------------------------------------------- */
#include <iostream>
#include "ComputeArithmetic.h"
#include "ComputePi.h"
#include "PrintSeries.h"
#include <memory>
#include <string>
#include "WriteSeries.h"
#include <sstream>
#include <exception>
#include <fstream>
#include <stdexcept>
#include "RiemannIntegral.h"
#include <iomanip>
#include <cmath>
int main(int argc, char ** argv) {

    if (argv[1] == std::string("Pi") || argv[1] == std::string("Arithmetic")) {
        // if not enough arguments, stop the program.
        if (argc < 6) {
            throw std::invalid_argument("The number of arguments should be at least 5");
        }
        // concatenate the arguments in argv
        std::stringstream sstr;
        for (int i = 1; i < argc; i++) {
            sstr << argv[i] << " ";
        }
        int fre, max, pre; //fre: frequency, max: maxiter, pre:precision
        std::string ser, ope, sep; //ser: series type, ope: operation(Print/Write), sep: separator
        sstr >> ser >> fre >> max >> pre >> ope >> sep;

        //exercise 2
        std::unique_ptr<Series> met = nullptr;
        //according to "ser", decide which class "met" points to
        if (ser == std::string("Arithmetic")) {
            met = std::make_unique<ComputeArithmetic>();
        } else if (ser == std::string("Pi")) {
            met = std::make_unique<ComputePi>();
        } else {
            throw std::invalid_argument("No such Series method");
        }
        //exercise 3
        //according to "ope", decide print or write results
        if (ope == std::string("Print")) {
            PrintSeries ps(fre, max, *met);
            ps.setPrecision(pre);
            ps.dump();
        } else if (ope == std::string("Write")) {
            WriteSeries ws(fre, max, *met, sep);
            ws.setPrecision(pre);
            ws.dump();
        }

        //exercise 4
        std::ofstream outfile;
        outfile.open("exercise5.txt", std::ios::trunc);
        PrintSeries ps5(fre, max, *met);
        ps5.setPrecision(pre);
        outfile << ps5; // "<<" operator has been overrided, write results to file

        //exercise 5
        //class "Series","ComputePi" and "ComputeArithmetic" are adjusted.
        //new methods "addTerm", "computeTerm" are added.
        //new members "current_index", "current_value" are added.
    }
        //exercise 6
    else if (argv[1] == std::string("Riemann")) {
        // if not enough arguments, stop the program.
        if (argc < 7) {
            throw std::invalid_argument("The number of arguments should be at least 5");
        }
        // concatenate the arguments in argv
        std::stringstream sstr;
        for (int i = 1; i < argc; i++) {
            sstr << argv[i] << " ";
        }
        double a, b;
        unsigned int N;
        int  pre; //fre: frequency, max: maxiter, pre:precision
        std::string ser, ftype; //ser: series type, ope: operation(Print/Write), sep: separator
        sstr >> ser >> a >> b >> N >> pre >> ftype;
        auto integ = std::make_unique<RiemannIntegral>(a, b, ftype);
        std::cout << std::scientific << std::setprecision(pre);
        std::cout <<"Numerical: " << integ->compute(N) << std::endl;
        std::cout <<"Analytic: " << integ->getAnalyticPrediction() << std::endl;
    }
}
