import matplotlib.pyplot as plt
from pathlib import Path
import os
if __name__ == '__main__':
    x = []
    numerical = []
    analytic = []
    fig = plt.figure()
    axe = fig.add_subplot(1, 1, 1)
    workpath = os.path.dirname(__file__)
    parentpath = Path(workpath)
    parentpath = parentpath.parent.absolute()
    filepath = os.path.join(parentpath, 'cmake-build-debug/exercise5.txt')
    with open(filepath, 'r') as f:
        lines = f.readlines()
    for line in lines:
        words = line.split()
        x.append(float(words[2]))
        numerical.append(float(words[5]))
        if len(words) > 7:
            analytic.append(float(words[7]))

    axe.plot(x, numerical, marker='o', label='Numerical')
    if analytic != []:
        axe.plot(x, numerical, marker='o', label='Numerical')
        axe.plot(x, analytic, label='Analytical')
    axe.set_xlabel(r'$k$')
    axe.set_ylabel(r'Series')
    axe.legend()
    plt.show()