-------------------------------------------------------------------
# Project Homework 3, Dec 2021
Authors:
Zheng Zheng    zheng.zheng@epfl.ch
Tian Yang      tian.yang@epfl.ch

-------------------------------------------------------------------
# Instructions to compile the code
Compilation:

mkdir build
cd build
ccmake ../
make
-------------------------------------------------------------------
# Answer Ex.1
Thre are three particle types here, with MaterialPoint being the additional one. They all belong to the Particles class and have a specific factory in ParticlesFactoryInterface class for future use.



-------------------------------------------------------------------
# Instructions to run the program:

In the build/src directory:

In order to get the help:

./compute_series --help

The arguments that can be provided are the following :

- --series_type : pi/arithmetic/integral (Default pi)


Examples:

- ./compute_series --series_type arithmetic --dumper_type write

- ./compute_series --series_type integral --function sin --dumper_type write --integral_bound_inf 0 --integral_bound_sup pi/2

-------------------------------------------------------------------


test heat equation, use the Python file to generate .csv files and copy them to the build folder, and make a directory dumps (mkdir dumps), then launch ./test_heat_equation
cannot open file dumps/step-00000.csv, why need to open folder dump?

make sure that Google test is installed before launching tests

test_kepler failed, I think due to the new format of output file
