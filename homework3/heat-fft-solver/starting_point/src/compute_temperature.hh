#ifndef __COMPUTE_TEMPERATURE__HH__
#define __COMPUTE_TEMPERATURE__HH__

/* -------------------------------------------------------------------------- */
#include "compute.hh"

//! Compute temperature evolution for one time step
class ComputeTemperature : public Compute {

public:
  // constructors
  ComputeTemperature(Real dt, Real L){
      this->L = L;
      this->dt = dt;
  };
  
  // compute temperature for next time step
  void compute(System& system) override;
  void setDeltaT(Real dt){this->dt = dt;}
  
private:
  Real dt; // time step
  Real L;  // Length of the x-axis and y-axis
};

/* -------------------------------------------------------------------------- */
#endif  //__COMPUTE_TEMPERATURE__HH__
