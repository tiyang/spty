#ifndef FFT_HH
#define FFT_HH
/* ------------------------------------------------------ */
#include "matrix.hh"
#include "my_types.hh"
#include <fftw3.h>
/* ------------------------------------------------------ */

struct FFT {

  static Matrix<complex> transform(Matrix<complex>& m);
  static Matrix<complex> itransform(Matrix<complex>& m);

  static Matrix<std::complex<int>> computeFrequencies(UInt size);
};

/* ------------------------------------------------------ */

inline Matrix<complex> FFT::transform(Matrix<complex>& m_in) {

  UInt N = m_in.size();
  Matrix<complex> m_out(N);
  
  fftw_complex *in, *out;
  fftw_plan p;
  in = (fftw_complex*) fftw_malloc(sizeof(fftw_complex)*N*N);
  out = (fftw_complex*) fftw_malloc(sizeof(fftw_complex)*N*N);

  // extract values from "m_in" to "in"
  for (auto && entry : index(m_in)) {
    int i = std::get<0>(entry);
    int j = std::get<1>(entry);
    auto& val = std::get<2>(entry);
    
    in[i*N + j][0] = val.real(); // "in" is row-major order matrix
    in[i*N + j][1] = val.imag();
  }

  // execute FFTW
  p = fftw_plan_dft_2d(N, N, in, out, FFTW_FORWARD, FFTW_ESTIMATE);
  fftw_execute(p);

  // extract values from "out" to "m_out"
  for (auto && entry : index(m_out)){
    int i = std::get<0>(entry);
    int j = std::get<1>(entry);
    auto& val = std::get<2>(entry);
    
    val.real(out[i*N + j][0]); // "out" is row-major order matrix
    val.imag(out[i*N + j][1]);
  }

  // clean up
  fftw_destroy_plan(p);
  fftw_cleanup();
  fftw_free(in);
  fftw_free(out);

  return m_out;
}

/* ------------------------------------------------------ */

inline Matrix<complex> FFT::itransform(Matrix<complex>& m_in) {

  UInt N = m_in.size();
  Matrix<complex> m_out(N);
  
  fftw_complex *in, *out;
  fftw_plan p;
  in = (fftw_complex*) fftw_malloc(sizeof(fftw_complex)*N*N);
  out = (fftw_complex*) fftw_malloc(sizeof(fftw_complex)*N*N);

  // extract values from "m_in" to "in"
  for (auto && entry : index(m_in)) {
    int i = std::get<0>(entry);
    int j = std::get<1>(entry);
    auto& val = std::get<2>(entry);
    
    in[i*N + j][0] = val.real(); // "in" is row-major order
    in[i*N + j][1] = val.imag();
  }

  // execute FFTW
  p = fftw_plan_dft_2d(N, N, in, out, FFTW_BACKWARD, FFTW_ESTIMATE);
  fftw_execute(p);

  // extract values from "out" to "m_out"
  for (auto && entry : index(m_out)){
    int i = std::get<0>(entry);
    int j = std::get<1>(entry);
    auto& val = std::get<2>(entry);
    
    val.real(out[i*N + j][0] / Real(N * N)); // "out" is row-major order, and added 1/N^2 factor 
    val.imag(out[i*N + j][1] / Real(N * N));
  }

  // clean up
  fftw_destroy_plan(p);
  fftw_cleanup();
  fftw_free(in);
  fftw_free(out);

  return m_out;

}

/* ------------------------------------------------------ */

inline Matrix<std::complex<int>> FFT::computeFrequencies(UInt size) {

  Matrix<std::complex<int>> wave_number(size);

  for (auto && entry : index(wave_number)){
    int i = std::get<0>(entry);
    int j = std::get<1>(entry);
    auto& val = std::get<2>(entry);

    // in i direction
    if (i < size/2.0){
      val.imag(i);
    }
    else{
      val.imag(i - size);
    }

    // in j direction
    if (j < size/2.0){
      val.real(j);
    }
    else{
      val.real(j - size);
    }
  }

  return wave_number;
}

#endif  // FFT_HH
