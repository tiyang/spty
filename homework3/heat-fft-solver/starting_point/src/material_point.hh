#ifndef __MATERIAL_POINT__HH__
#define __MATERIAL_POINT__HH__

/* -------------------------------------------------------------------------- */
#include "particle.hh"

//! Class for MaterialPoint
class MaterialPoint : public Particle {
  /* ------------------------------------------------------------------------ */
  /* Methods                                                                  */
  /* ------------------------------------------------------------------------ */

public:

  void printself(std::ostream& stream) const override;
  void initself(std::istream& sstr) override;

  Real & getTemperature(){return temperature;};
  Real & getHeatRate(){return heat_rate;};
  Real & getMassDensity() {return rho;};
  Real & getHeatCapacity() {return heat_capacity;};
  Real & getHeatConductivity() {return kappa;};
  
private:
  Real temperature;
  Real heat_rate;
  Real rho; 		  // mass density
  Real heat_capacity; // heat capacity
  Real kappa; 		  // heat conductivity
};

/* -------------------------------------------------------------------------- */
#endif  //__MATERIAL_POINT__HH__
