import numpy as np

import argparse

parser = argparse.ArgumentParser()
parser.add_argument("--size", type=int, action = "store",default = 16, help="the number of particles in one direction")
parser.add_argument("--filename",  type = str, action = "store", default = "input.csv", help="name of generated input file")
parser.add_argument("--heatsource", type = str, action = "store", choices=["uniform", "radial", "delta", "sinusoidal"],
                       default='uniform', help="the type of heat source")
parser.add_argument("--halflength", type = float, action = "store", default = 1.0, help="(x,y) belongs in [-hl,hl)")
parser.add_argument("--temperature", type = float, action = "store", default = 1.0, help="initial temperature")
parser.add_argument("--massdensity", type = float, action = "store", default = 1.0, help="mass density")
parser.add_argument("--heatcapacity", type = float, action = "store", default = 1.0, help="heat capacity")
parser.add_argument("--kappa",  type = float, action = "store", default = 1.0, help="heat conductivity")
parser.add_argument("--radius",  type = float, action = "store", default = 1.0, help="radius of radical heat source")

args = parser.parse_args()

# get datas from argparse
size = args.size
filename = args.filename
type = args.heatsource
hl = args.halflength
rho = args.massdensity
hc = args.heatcapacity
kappa = args.kappa
temperature = args.temperature
r = args.radius


dx = 2*hl/size #interval between two adjacent particles
x = np.linspace(-hl, hl-dx,size)
y = np.linspace(-hl, hl-dx,size)

# matrix of positions
X, Y = np.meshgrid(x, y)
Z = np.zeros((size, size))

# matrix of parameters
RHO = rho*np.ones((size,size))
HC = hc*np.ones((size,size))
KAPPA = kappa*np.ones((size,size))

#Generate temperature and heat source values for each particle
#based on the type of heat source
if type == "uniform":
    T = temperature * np.ones((size, size))  # generate uniform temperature 1.0 for all particles
    HS = np.zeros((size,size))
elif type == "sinusoidal":
    T = temperature * np.ones((size, size))
    HS = np.zeros((size,size))
    for i in range(0,size):
        for j in range(0,size):
            x0 = X[i,j]
            HS[i,j] = pow((np.pi/hl),2)* np.sin(np.pi / hl * x0)
            T[i,j] = np.sin(np.pi / hl * x0)
elif type == "delta":
    T = temperature * np.ones((size, size))
    HS = np.zeros((size, size))
    for i in range(0,size):
        for j in range(0,size):
            x0 = X[i,j]
            if x0 <= -0.5:
                T[i,j] = -x0-1
                if x0 == -0.5:
                    HS[i,j] = -1
            elif x0 > -0.5 and x0 <= 0.5:
                T[i,j] = x0
                if x0 == 0.5:
                    HS[i,j] = 1
            elif x0 > 0.5:
                T[i,j] = -x0+1
elif type == "radial":
    T = temperature * np.ones((size, size))
    HS = np.zeros((size, size))
    HS[np.where((X**2 + Y**2) < r)] = 1

# change 2D matrix into 1D array, in row-major order
X = X.flatten(order='C')
Y = Y.flatten(order='C')
Z = Z.flatten(order='C')
T = T.flatten(order='C')
HS = HS.flatten(order='C')
RHO = RHO.flatten(order='C')
HC = HC.flatten(order='C')
KAPPA = KAPPA.flatten(order='C')

#print results to output file
file_data = np.column_stack((X,Y,Z,T,HS,RHO,HC,KAPPA))
np.savetxt(filename, file_data, delimiter=" ")
